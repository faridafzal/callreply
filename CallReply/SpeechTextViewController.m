//
//  SpeechTextViewController.m
//  CallReply
//
//  Created by apple on 10/6/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "SpeechTextViewController.h"
#import "AppDelegate.h"
#import "UserInfo.h"
#import "Text.h"
#import "Toast+UIView.h"
#import <CoreText/CoreText.h>
#import <MessageUI/MessageUI.h>
#import <QuickLook/QuickLook.h>
#import "Constants.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "SubscriptionViewController.h"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone)
//#define kDefaultPageHeight 792
//#define kDefaultPageWidth  612
#define kDefaultPageHeight 841.68
#define kDefaultPageWidth  595.44

@interface SpeechTextViewController () {
    Text *text;
    NSString *placeHolder;
    NSString *previousText;
    NSString *nextString;
    NSString* pdfFilePath;
    BOOL isEdited;
    int i;
    NSRange selctedRange;
    UserInfo *userInfo;
    BOOL isRecording;
    NSTimer *timer;
    int timeSec;
    int timeMin;
    BOOL print;
    BOOL isFileCreated;
    NSInteger currentPageNumber;
    NSString* createdFileName;
}

@end

@implementation SpeechTextViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.textView.returnKeyType = UIReturnKeyDone;
    isRecording = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadAlertView:)
                                                 name:@"ExpireSession"
                                               object:nil];

    previousText = @"";
    nextString = @"";
    createdFileName = @"";
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(hideKeyboard:)];
    [self.view addGestureRecognizer:singleFingerTap];
    [[self.textView layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.textView layer] setBorderWidth:0.5];
    self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backBtn"] style:UIBarButtonItemStylePlain target:self action:@selector(handleBack:)];
    currentPageNumber = 0;
    print = false;
//    userInfo.isAutoSaveEnabled = true;
    
  
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    timeSec = 0;
    placeHolder = @"Press Start Recording to record text";
    self.title = @"Speech To Text";
    userInfo = [UserInfo instance];
    self.autoSaveButton.selected = true;
    isFileCreated = NO;
    
    if (self.isNewRecording) {
        i = 0;
        isEdited = NO;
        [self.textView setEditable:NO];
        userInfo.audioSessionForVoice = false;
        [userInfo saveUserInfo];
    //    self.navigationItem.rightBarButtonItem = self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"share"] style:UIBarButtonItemStylePlain target:self action:@selector(shareText:)];
        _textView.delegate = self;
        self.textView.text = placeHolder;
        [self setPageNumber];
    }
    else {
        isEdited = NO;
        [self.textView setEditable: YES];
        previousText =  self.formerText.text;
        self.textView.text = previousText;
        _textView.delegate = self;
        NSLog(@"NAME %@",self.formerText.name);
        createdFileName = self.formerText.name;
        [self setPageNumber];
        i = 1;
    }
    recognizer = [[SFSpeechRecognizer alloc] initWithLocale:[NSLocale localeWithLocaleIdentifier:@"en-US"]];
    [recognizer setDelegate:self];
    [SFSpeechRecognizer requestAuthorization:^(SFSpeechRecognizerAuthorizationStatus authStatus) {
        
        switch (authStatus) {
            case SFSpeechRecognizerAuthorizationStatusAuthorized:
                //User gave access to speech recognition
                NSLog(@"Authorized");
                self.microPhoneButton.enabled = YES;
                break;
                
            case SFSpeechRecognizerAuthorizationStatusDenied:
                //User denied access to speech recognition
                NSLog(@"SFSpeechRecognizerAuthorizationStatusDenied");
                break;
                
            case SFSpeechRecognizerAuthorizationStatusRestricted:
                //Speech recognition restricted on this device
                NSLog(@"SFSpeechRecognizerAuthorizationStatusRestricted");
                break;
                
            case SFSpeechRecognizerAuthorizationStatusNotDetermined:
                //Speech recognition not yet authorized
                NSLog(@"SFSpeechRecognizerAuthorizationStatusNotDetermined");
                break;
                
            default:
                NSLog(@"Default");
                break;
        }
    }];
    audioEngine = [[AVAudioEngine alloc] init];
    speechSynthesizer  = [[AVSpeechSynthesizer alloc] init];
    
   
}

- (void)viewDidDisssppear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)CreateFile {
    
    [self.view makeToastActivity];
    NSDateFormatter *formatter;
    NSString *dateString;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    //[formatter setDateFormat:@"dd/MM/yyyy"];
    dateString = [formatter stringFromDate:[NSDate date]];
    NSDate *time = [formatter dateFromString:dateString];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"/" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@":" withString:@""];
    dateString = [NSString stringWithFormat:@"%@%@",@"UR_Text_", dateString];
    
    //MEEEEE
    createdFileName = dateString;
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *textToSave = [NSEntityDescription insertNewObjectForEntityForName:@"Text" inManagedObjectContext:context];
    [textToSave setValue:self.textView.text forKey:@"text"];
    [textToSave setValue:time forKey:@"time"];
    if(!self.isNewRecording){
        if([self.formerText.isNameChanged isEqualToString:@"YES"]){
            [textToSave setValue:@"YES" forKey:@"isNameChanged"];
            [textToSave setValue:self.formerText.name forKey:@"name"];
        }
        else{
            [textToSave setValue:@"NO" forKey:@"isNameChanged"];
            [textToSave setValue:dateString forKey:@"name"];
        }
    }else{
        [textToSave setValue:@"NO" forKey:@"isNameChanged"];
        [textToSave setValue:dateString forKey:@"name"];
    }
    
    NSError *error = nil;
    if (![textToSave.managedObjectContext save:&error]) {
        NSLog(@"Save Failed! %@ %@", error, [error localizedDescription]);
    }else{
        
        NSLog(@"File Save! %@ %@", error, [error localizedDescription]);
    }

}

-(void)saveCurrentTextViewText {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Text" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
//        NSLog(@"%@", result);
        for(NSManagedObject *textToSave in result){
            
            if([[textToSave valueForKey:@"name"] isEqualToString:createdFileName]){
                
                [textToSave setValue:self.textView.text forKey:@"text"];
                [textToSave setValue:@"NO" forKey:@"isNameChanged"];
                [textToSave setValue:createdFileName forKey:@"name"];
                NSError *saveError = nil;
                
                if (![textToSave.managedObjectContext save:&saveError]) {
                    NSLog(@"Unable to save managed object context.");
                    NSLog(@"%@, %@", saveError, saveError.localizedDescription);
                }else {
                    NSLog(@"UPDATE SUCCESS");
                }
            }
        }
//        NSManagedObject *textToSave = (NSManagedObject *)[result objectAtIndex:0];
    }
}


-(void)startRecording:(NSString*)firstString{
    
    userInfo = [UserInfo instance];
    userInfo.audioSessionForText = true;
    [userInfo saveUserInfo];
    [self.textView setEditable:YES];
    if (currentTask != nil) {
        [currentTask cancel];
        currentTask = nil;
    }
    NSError * outError;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryRecord error:&outError];
    [audioSession setMode:AVAudioSessionModeMeasurement error:&outError];
    [audioSession setActive:true withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation  error:&outError];
    recognitionRequest = [[SFSpeechAudioBufferRecognitionRequest alloc] init];
    inputNode = [audioEngine inputNode];
    if (recognitionRequest == nil) {
        NSLog(@"Unable to created a SFSpeechAudioBufferRecognitionRequest object");
    }
    if (inputNode == nil) {
        
        NSLog(@"Unable to created a inputNode object");
    }
    recognitionRequest.shouldReportPartialResults = true;
    currentTask = [recognizer recognitionTaskWithRequest:recognitionRequest resultHandler:^(SFSpeechRecognitionResult * _Nullable result, NSError * _Nullable error) {
        BOOL isFinal = NO;
        
        if (result != nil) {
            
            isRecording = YES;

            self.textView.text = [NSString stringWithFormat:@"%@ %@ %@",firstString,result.bestTranscription.formattedString,nextString];
            
                NSString *trimmedString = [self.textView.text stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString *str1;
                str1 = [trimmedString stringByAppendingString:@" "];
                self.textView.text = str1;
           
            if(![previousText isEqual:@""])
            {

                [self.textView setSelectedRange:NSMakeRange(1 + firstString.length + result.bestTranscription.formattedString.length, 0)];
                
                if(userInfo.isAutoSaveEnabled){
                    
                    if(![previousText isEqualToString:self.textView.text]){
                        
                        [self saveCurrentTextViewText];
                    }
                }
                
            }else{
                
                if(userInfo.isAutoSaveEnabled){

                    if(([firstString isEqualToString:@""]) && isFileCreated == NO){
                        
                        [self CreateFile];
                        isFileCreated = YES;
                        
                    }else {
                    
                        if(![previousText isEqualToString:self.textView.text]){
                            
                            [self saveCurrentTextViewText];
                        }
                    }
                }
                
                [self.textView setSelectedRange:NSMakeRange(0 + firstString.length + result.bestTranscription.formattedString.length, 0)];
            }
            
            
              [self.textView setSelectedRange:NSMakeRange(1 + firstString.length + self.textView.text.length, 0)];
            
//            NSLog(@"text is:%@", self.textView.text);
            [self setPageNumber];
            isFinal = result.isFinal;
        }
        
        
        if (error != nil || isFinal ){
            [audioEngine stop];
            [inputNode removeTapOnBus:0];
            recognitionRequest = nil;
            currentTask = nil;
            isRecording = NO;
           // [self.microPhoneButton  setTitle:@"Start Recording" forState:UIControlStateNormal];
            UIImage *closebtnimg = [UIImage imageNamed:@"newstart"];
            [self.microPhoneButton setImage: closebtnimg forState:UIControlStateNormal];

           // dispatch_async(dispatch_get_main_queue(), ^{
               // [self.view hideToastActivity];
                [self clearText];
          //  });

        } else if (error) {
            [audioEngine stop];
            [inputNode removeTapOnBus:0];
            recognitionRequest = nil;
            currentTask = nil;
            isRecording = NO;
           // [self.microPhoneButton  setTitle:@"Start Recording" forState:UIControlStateNormal];
            UIImage *closebtnimg = [UIImage imageNamed:@"newstart"];
            [self.microPhoneButton setImage: closebtnimg forState:UIControlStateNormal];
            [self clearText];
        }
    }];
    
    [inputNode installTapOnBus:0 bufferSize:1024 format:[inputNode outputFormatForBus:0] block:^(AVAudioPCMBuffer *buffer, AVAudioTime *when){
//        NSLog(@"Block tap!");
        [recognitionRequest appendAudioPCMBuffer:buffer];
        
    }];
    [audioEngine prepare];
    [audioEngine startAndReturnError:&outError];
    NSLog(@"Error %@", outError);
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if(isRecording)
    {
        if(self.textView.inputView)
        {
            
        }
        else
        {
            [self invalidateTimer];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (isRecording) {
                    [recognitionRequest endAudio];
                    [audioEngine stop];
                    [inputNode reset];
                    [currentTask cancel];
                    isRecording = NO;
                    [inputNode removeTapOnBus:0];
                    recognitionRequest = nil;
                    currentTask = nil;
                }
               // [self.microPhoneButton  setTitle:@"Start Recording" forState:UIControlStateNormal];
                UIImage *closebtnimg = [UIImage imageNamed:@"newstart"];
                [self.microPhoneButton setImage: closebtnimg forState:UIControlStateNormal];            });
        }
    }
//    [self invalidateTimer];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        if (isRecording) {
//            [recognitionRequest endAudio];
//            [audioEngine stop];
//            [inputNode reset];
//            [currentTask cancel];
//            isRecording = NO;
//            [inputNode removeTapOnBus:0];
//            recognitionRequest = nil;
//            currentTask = nil;
//        }
//        [self.microPhoneButton  setTitle:@"Start Recording" forState:UIControlStateNormal];
//    });
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
        isEdited = YES;
        previousText = self.textView.text;
        selctedRange = textView.selectedRange;
        NSInteger indexPoint = selctedRange.location;
        nextString = [previousText substringFromIndex:indexPoint];
        previousText = [previousText substringToIndex:indexPoint];
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(void)updateCursorText
{
    isEdited = YES;
    previousText = self.textView.text;
    selctedRange = self.textView.selectedRange;
    NSInteger indexPoint = selctedRange.location;
    nextString = [previousText substringFromIndex:indexPoint];
    previousText = [previousText substringToIndex:indexPoint];
}

- (void)hideKeyboard:(UITapGestureRecognizer *)recognizer {
    if(!self.textView.isUserInteractionEnabled)
    {
        NSLog(@"Print do not hide keyboard");
    }
    else
    {
        if(self.textView.isEditable) {
            isEdited = YES;
            previousText = self.textView.text;
            selctedRange = self.textView.selectedRange;
            NSInteger indexPoint = selctedRange.location;
            nextString = [previousText substringFromIndex:indexPoint];
            previousText = [previousText substringToIndex:indexPoint];
            [self.view endEditing:YES];
        }
    }
}

- (IBAction)textViewTapAction:(id)sender {
    if(isRecording)
    {
        [self invalidateTimer];
        [self.textView resignFirstResponder];
        self.textView.inputView = nil;
        [self.textView setUserInteractionEnabled:YES];
        [self updateCursorText];
        [self.textView becomeFirstResponder];
        
        [recognitionRequest endAudio];
        [audioEngine stop];
        [inputNode removeTapOnBus:0];
        [inputNode reset];
        [currentTask cancel];
        isRecording = NO;
       // [self.microPhoneButton  setTitle:@"Start Recording" forState:UIControlStateNormal];
        UIImage *closebtnimg = [UIImage imageNamed:@"newstart"];
        [self.microPhoneButton setImage: closebtnimg forState:UIControlStateNormal];
        [self.textViewTapButton setHidden:true];
    }
}

- (void)textViewTapped:(UITapGestureRecognizer *)recognizer {
    if(isRecording)
    {
        [self invalidateTimer];
        [self.textView resignFirstResponder];
        self.textView.inputView = nil;
        [self.textView setUserInteractionEnabled:YES];
        [self updateCursorText];
        [self.textView becomeFirstResponder];

        [recognitionRequest endAudio];
        [audioEngine stop];
        [inputNode removeTapOnBus:0];
        [inputNode reset];
        [currentTask cancel];
        isRecording = NO;
      //  [self.microPhoneButton  setTitle:@"Start Recording" forState:UIControlStateNormal];
        UIImage *closebtnimg = [UIImage imageNamed:@"newstart"];
        [self.microPhoneButton setImage: closebtnimg forState:UIControlStateNormal];
//        [self invalidateTimer];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            if (isRecording) {
//                [recognitionRequest endAudio];
//                [audioEngine stop];
//                [inputNode reset];
//                [currentTask cancel];
//                isRecording = NO;
//                [inputNode removeTapOnBus:0];
//                recognitionRequest = nil;
//                currentTask = nil;
//            }
//            [self.microPhoneButton  setTitle:@"Start Recording" forState:UIControlStateNormal];
//        });
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self setPageNumber];
}

- (IBAction)microPhoneTapped {
    if (i == 0)
    {
        self.textView.text = @"";
        [self setPageNumber];
    }
    
    i = i + 1;
    if (isRecording) {
        self.textView.inputView = nil;
        [self.textView setUserInteractionEnabled:YES];
        [self.textView resignFirstResponder];
        [self updateCursorText];
        [recognitionRequest endAudio];
        [audioEngine stop];
        [inputNode removeTapOnBus:0];
        [inputNode reset];
        [currentTask cancel];
        isRecording = NO;
       // [self.microPhoneButton  setTitle:@"Start Recording" forState:UIControlStateNormal];
        UIImage *closebtnimg = [UIImage imageNamed:@"newstart"];
        [self.microPhoneButton setImage: closebtnimg forState:UIControlStateNormal];

    } else {
        UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        self.textView.inputView = dummyView;
        [self.textView setEditable:YES];
        [self.textView setUserInteractionEnabled:YES];
        [self.textView becomeFirstResponder];
        [self.textViewTapButton setHidden:NO];
        isRecording = YES;
        if (isEdited) {
            
            [self startRecording:previousText];
            isEdited = NO;
            previousText = self.textView.text;
        } else {
            nextString = @"";
            previousText = self.textView.text;
            [self startRecording:previousText];
        }
       // _microPhoneButton.hidden = NO;
      //  [self.microPhoneButton  setTitle:@"Stop Recording" forState:UIControlStateNormal];
        UIImage *closebtnimg = [UIImage imageNamed:@"newstop"];
        [self.microPhoneButton setImage: closebtnimg forState:UIControlStateNormal];

    }
}

- (IBAction)clearTextView:(id)sender {
    self.microPhoneButton.userInteractionEnabled = NO;
    self.microPhoneButton.enabled = NO;
    [self invalidateTimer];
    self.timeLabel.text = @"00:00";
    [self.textView setEditable:NO];
    [self.view makeToastActivity];
    userInfo = [UserInfo instance];
    userInfo.audioSessionForText = NO;
    [userInfo saveUserInfo];
    previousText = @"";
    nextString = @"";
    i = 0;
   dispatch_async(dispatch_get_main_queue(), ^{
        isEdited = NO;
        if(isRecording){
            [recognitionRequest endAudio];
            [audioEngine stop];
            [inputNode removeTapOnBus:0];
            [inputNode reset];
            [currentTask cancel];
            isRecording = NO;
        } else {
                [NSTimer scheduledTimerWithTimeInterval: 1.5
                                                 target: self
                                                            selector:@selector(clearText)
                                                            userInfo: nil repeats:NO];
        }
       currentTask = nil;
        recognitionRequest = nil;
      //  [self.microPhoneButton  setTitle:@"Start Recording" forState:UIControlStateNormal];
       UIImage *closebtnimg = [UIImage imageNamed:@"newstart"];
       [self.microPhoneButton setImage: closebtnimg forState:UIControlStateNormal];
    });
}

- (IBAction)printText:(id)sender {
    if([self.pageNo.text isEqualToString:@"Updating..."])
    {
        if(!print)
        {
            print = YES;
            [self.view makeToastActivity];
        }
    }
    else{
        print = NO;
        [self.view hideToastActivity];
        [self printDocument];
    }
    
}

- (IBAction)clearSession {
    dispatch_async(dispatch_get_main_queue(), ^{
        previousText = @"";
        nextString = @"";
        if (isRecording) {
            [recognitionRequest endAudio];
            [audioEngine stop];
            [inputNode removeTapOnBus:0];
            [inputNode reset];
            [currentTask cancel];
            isRecording = NO;
        }
        recognitionRequest = nil;
        currentTask = nil;
    //    [self.microPhoneButton  setTitle:@"Start Recording" forState:UIControlStateNormal];
        UIImage *closebtnimg = [UIImage imageNamed:@"newstart"];
        [self.microPhoneButton setImage: closebtnimg forState:UIControlStateNormal];        [self.navigationController popViewControllerAnimated:YES];
    });
}


- (void) clearText {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog (@"clearing text");
        userInfo = [UserInfo instance];
        if (!userInfo.audioSessionForText){
            i = 0;
            self.textView.text = placeHolder;
            [self setPageNumber];
            previousText = @"";
            nextString = @"";
        }
        self.microPhoneButton.enabled = YES;
        self.microPhoneButton.userInteractionEnabled = YES;
        [self.view hideToastActivity];
    });
}

- (void)shareText:(id)sender{
    [self invalidateTimer];
    NSString *textToShare = self.textView.text;
    dispatch_async(dispatch_get_main_queue(), ^{
    if (isRecording) {
        [recognitionRequest endAudio];
        [audioEngine stop];
        [inputNode removeTapOnBus:0];
        [inputNode reset];
        [currentTask cancel];
        isRecording = NO;
      //  [self.microPhoneButton  setTitle:@"Start Recording" forState:UIControlStateNormal];
        UIImage *closebtnimg = [UIImage imageNamed:@"newstart"];
        [self.microPhoneButton setImage: closebtnimg forState:UIControlStateNormal];    }});
    if ([textToShare isEqualToString:@""] || i == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please record text to share"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];        
    } else {
        NSArray *activityItems;
        activityItems = @[textToShare];
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        [activityViewController setValue:@"Speech Text" forKey:@"subject"];
        if (IS_IPAD) {
            activityViewController.popoverPresentationController.sourceView = self.view;
        }
        [activityViewController setCompletionHandler:^(NSString *activityType, BOOL completed) {
            if (!completed) {
                return;
            }
            else{
                userInfo = [UserInfo instance];
                userInfo.numberOfFreeTexts = userInfo.numberOfFreeTexts + 1;
                [userInfo saveUserInfo];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self clearTextView:self];
                });
                if (!userInfo.isSubscribed){
                    int remainingFreeRecordings = kNumberOfFreeTextsCount - userInfo.numberOfFreeTexts;
                    NSString *message = @"";
                    if (remainingFreeRecordings == 0) {
                        message = @"Your trial recroding are over now.You need to subscribe to use full features of \"Dragon Speech Recording\"";
                    } else {
                        message = [NSString stringWithFormat:@"Your %d trial text recordings are left.Make sure to get subscribed to share unlimited text when your trial peroid is over",remainingFreeRecordings];
                    }
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                    message: message
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
            }
        }];
        [self presentViewController:activityViewController animated:YES completion:nil];
    }
}

-(void)GoBack {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        previousText = @"";
        nextString = @"";
        
        if (isRecording) {
            
            [recognitionRequest endAudio];
            [audioEngine stop];
            [inputNode removeTapOnBus:0];
            [inputNode reset];
            [currentTask cancel];
            isRecording = NO;
        }
        
        recognitionRequest = nil;
        currentTask = nil;
        // [self.microPhoneButton  setTitle:@"Start Recording" forState:UIControlStateNormal];
        UIImage *closebtnimg = [UIImage imageNamed:@"newstart"];
        [self.microPhoneButton setImage: closebtnimg forState:UIControlStateNormal];
        userInfo = [UserInfo instance];
        userInfo.audioSessionForText = NO;
        [userInfo saveUserInfo];
        [self.navigationController popViewControllerAnimated:YES];
    });

}

- (void)handleBack:(id)sender{
    
    userInfo = [UserInfo instance];
    
    if(userInfo.isAutoSaveEnabled){

        if(isEdited == NO){
            
            [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(GoBack) userInfo:nil repeats:NO];
            return;
        }
        
        userInfo.numberOfFreeTexts = userInfo.numberOfFreeTexts + 1;
        userInfo.audioSessionForText = NO;
        [userInfo saveUserInfo];
        [self.view hideToastActivity];
//        [self deleteText];
        NSString *message = @"";
        int remainingFreeRecordings = kNumberOfFreeTextsCount - userInfo.numberOfFreeTexts;
        if (!userInfo.isSubscribed) {
            if (remainingFreeRecordings == 0) {
                message = @"Youe trial recording are over now.You need to subscribe to use full features of \"Dragon Speech Recording\"";
            } else {
                message = [NSString stringWithFormat:@"Your text is saved.Your can make %d trial text more.Make sure to get subscribed to make unlimited audios when your trial is over",remainingFreeRecordings];
            }
            
        } else {
            message = @"Your Text is saved";
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message: message delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(GoBack) userInfo:nil repeats:NO];
        return;
        
    }else {

        dispatch_async(dispatch_get_main_queue(), ^{
            previousText = @"";
            nextString = @"";
            if (isRecording) {
                [recognitionRequest endAudio];
                [audioEngine stop];
                [inputNode removeTapOnBus:0];
                [inputNode reset];
                [currentTask cancel];
                isRecording = NO;
            }
            recognitionRequest = nil;
            currentTask = nil;
           // [self.microPhoneButton  setTitle:@"Start Recording" forState:UIControlStateNormal];
            UIImage *closebtnimg = [UIImage imageNamed:@"newstart"];
            [self.microPhoneButton setImage: closebtnimg forState:UIControlStateNormal];
            userInfo = [UserInfo instance];
            userInfo.audioSessionForText = NO;
            [userInfo saveUserInfo];
            [self.navigationController popViewControllerAnimated:YES];
        });
    }

}

- (void) loadAlertView:(id)sender {
    userInfo = [UserInfo instance];
    dispatch_async(dispatch_get_main_queue(), ^{
        if(userInfo.audioSessionForText) {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:@"By doing this your current recording will become empty"
                                         preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Ok"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            [self clearSession];
                                        }];
            
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:yesButton];
            [alert addAction:noButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    });
    
}

- (IBAction)makeUserSession:(id)sender {
    userInfo = [UserInfo instance];
    [self invalidateTimer];
    if (userInfo.isSubscribed || userInfo.numberOfFreeTexts < kNumberOfFreeTextsCount) {
        if (![recognizer isAvailable]) {
            [self clearTextView:sender];
            [self showSpeechRecognitionError];
        } else {
            if(!isRecording){
                timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
            } else {
                [self.view makeToastActivity];
                self.microPhoneButton.enabled = NO;
                self.microPhoneButton.userInteractionEnabled = NO;
            }
            [self microPhoneTapped];
        }
    } else {
        [self askUserForSubscription];
    }
}
#pragma mark - helper methods
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    context = [delegate managedObjectContext];
    return context;
}

-(AppDelegate*)appDelegate{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}

- (IBAction)didTapAutoSave:(id)sender {
    
    UIButton *BTN = (UIButton *)sender;
    
    BTN.selected = !BTN.isSelected;
    
    userInfo = [UserInfo instance];
    userInfo.isAutoSaveEnabled = BTN.isSelected;
    [userInfo saveUserInfo];
    
}

- (IBAction)saveText:(id)sender {
    [self invalidateTimer];
    NSString *textToShare = self.textView.text;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (isRecording) {
            [recognitionRequest endAudio];
            [audioEngine stop];
            [inputNode removeTapOnBus:0];
            [inputNode reset];
            [currentTask cancel];
            isRecording = NO;
         //   [self.microPhoneButton  setTitle:@"Start Recording" forState:UIControlStateNormal];
            UIImage *closebtnimg = [UIImage imageNamed:@"newstart"];
            [self.microPhoneButton setImage: closebtnimg forState:UIControlStateNormal];        }});
    if ([textToShare isEqualToString:@""] || i == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please record text to save"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        
    } else {
        [self.view makeToastActivity];
        NSDateFormatter *formatter;
        NSString *dateString;
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
        //[formatter setDateFormat:@"dd/MM/yyyy"];
        dateString = [formatter stringFromDate:[NSDate date]];
        NSDate *time = [formatter dateFromString:dateString];
        dateString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        dateString = [dateString stringByReplacingOccurrencesOfString:@"/" withString:@""];
        dateString = [dateString stringByReplacingOccurrencesOfString:@":" withString:@""];
        dateString = [NSString stringWithFormat:@"%@%@",@"UR_Text_", dateString];
        NSManagedObjectContext *context = [self managedObjectContext];
        NSManagedObject *textToSave = [NSEntityDescription insertNewObjectForEntityForName:@"Text" inManagedObjectContext:context];
        [textToSave setValue:self.textView.text forKey:@"text"];
        [textToSave setValue:time forKey:@"time"];
        if(!self.isNewRecording){
            if([self.formerText.isNameChanged isEqualToString:@"YES"]){
                [textToSave setValue:@"YES" forKey:@"isNameChanged"];
                [textToSave setValue:self.formerText.name forKey:@"name"];
            }
            else{
                [textToSave setValue:@"NO" forKey:@"isNameChanged"];
                [textToSave setValue:dateString forKey:@"name"];
            }
        }else{
            [textToSave setValue:@"NO" forKey:@"isNameChanged"];
            [textToSave setValue:dateString forKey:@"name"];
       }
        
        NSError *error = nil;
        if (![textToSave.managedObjectContext save:&error]) {
            NSLog(@"Save Failed! %@ %@", error, [error localizedDescription]);
        }else{
            userInfo = [UserInfo instance];
            userInfo.numberOfFreeTexts = userInfo.numberOfFreeTexts + 1;
            userInfo.audioSessionForText = NO;
            [userInfo saveUserInfo];
            [self.view hideToastActivity];
            [self deleteText];
            NSString *message = @"";
            int remainingFreeRecordings = kNumberOfFreeTextsCount - userInfo.numberOfFreeTexts;
            if (!userInfo.isSubscribed) {
                if (remainingFreeRecordings == 0) {
                    message = @"Youe trial recording are over now.You need to subscribe to use full features of \"Dragon Speech Recording\"";
                } else {
                    message = [NSString stringWithFormat:@"Your text is saved.Your can make %d trial text more.Make sure to get subscribed to make unlimited audios when your trial is over",remainingFreeRecordings];
                }
                
            } else {
                message = @"Your Text is saved";
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message: message delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [alert show];
        
           [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(GoBack) userInfo:nil repeats:NO];
        }
    }
}

- (void)deleteText {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *audioEntity=[NSEntityDescription entityForName:@"Text" inManagedObjectContext:context];
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:audioEntity];
    NSPredicate *p=[NSPredicate predicateWithFormat:@"time == %@", self.formerText.time];
    [fetch setPredicate:p];
    NSError *fetchError;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *textToDelete in fetchedProducts) {
        [context deleteObject:textToDelete];
        NSError *error = nil;
        if (![context save:&error]){
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            
        }
    }
}

- (void)showSpeechRecognitionError {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Error"
                                 message:@"Speech recognition is not available. Please make sure internet is available and dictation is enabled from keyboard settings."
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* settings = [UIAlertAction
                                actionWithTitle:@"Settings"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
                                        
                                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:^(BOOL success) {
                                            NSLog(@"URL opened");
                                        }];
                                    }
                                }];

    UIAlertAction* ok= [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:nil];
    [alert addAction:ok];
    [alert addAction:settings];

    [self presentViewController:alert animated:YES completion:nil];
}

- (void)askUserForSubscription {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Subscription"
                                 message:@"Please subscribe to use full features of \"Dragon Speech Recording\""
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* subscribe = [UIAlertAction
                                actionWithTitle:@"Subscribe"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    SubscriptionViewController *subscriptionViewController = [[SubscriptionViewController alloc] init];
                                    [self.navigationController pushViewController:subscriptionViewController animated:YES];
//                                    [((AppDelegate*)[[UIApplication sharedApplication] delegate]) getSubscribed];
                                }];
    UIAlertAction* retore = [UIAlertAction
                               actionWithTitle:@"Restore"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   [((AppDelegate*)[[UIApplication sharedApplication] delegate]) restoreSubscription];
                               }];
    UIAlertAction* cancel = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    [alert addAction:subscribe];
    [alert addAction:retore];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)timerTick:(NSTimer *)timer {
    timeSec++;
    self.timeLabel.text = [NSString stringWithFormat:@"%02d:%02d", timeMin, timeSec];
    if (timeSec == 60) {
        timeSec = 0;
        [self timeOut];
        timeSec = 0;
    }
}

- (void)invalidateTimer {
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
    timeSec = 0;
    timeMin = 0;
    self.timeLabel.text = [NSString stringWithFormat:@"%02d:%02d", timeMin, timeSec];
}

- (void) timeOut {
//    [self invalidateTimer];
//    [self.view makeToastActivity];
//    dispatch_async(dispatch_get_main_queue(), ^{
//    previousText = self.textView.text;
//    nextString = @"";
//    if (isRecording) {
//        [recognitionRequest endAudio];
//        [audioEngine stop];
//        [inputNode removeTapOnBus:0];
//        [inputNode reset];
//        [currentTask cancel];
//        isRecording = NO;
//    }
//    });
//    recognitionRequest = nil;
//    currentTask = nil;
//    [self.microPhoneButton setTitle:@"Start Recording" forState:UIControlStateNormal];
    [self invalidateTimer];
    [self.view makeToastActivity];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (isRecording) {
            [recognitionRequest endAudio];
            [audioEngine stop];
            [inputNode removeTapOnBus:0];
            [inputNode reset];
            [currentTask cancel];
            isRecording = NO;
            
            recognitionRequest = nil;
            currentTask = nil;
        }
      //  [self.microPhoneButton  setTitle:@"" forState:UIControlStateNormal];
        UIImage *closebtnimg = [UIImage imageNamed:@"newstart"];
        [self.microPhoneButton setImage: closebtnimg forState:UIControlStateNormal];

        [self playBeep];
        self.textView.inputView = nil;
        [self.textView setUserInteractionEnabled:YES];
        [self.textView resignFirstResponder];
        [self updateCursorText];
        [self.textViewTapButton setHidden:true];
    });
}

- (void)playBeep
{
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"BEEP" ofType:@"wav"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    self.player.delegate = self;
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self.player setVolume:1.0];
    [_player prepareToPlay];
    
    [_player prepareToPlay];
    [self.player setCurrentTime:0];
    //[[AVAudioSession sharedInstance] setActive: YES error: nil];
    bool successPlay = [_player play];
    NSLog(@"BOOL SUccess play: %d",successPlay);
}


- (CFRange)renderPage:(NSInteger)pageNum withTextRange:(CFRange)currentRange

       andFramesetter:(CTFramesetterRef)framesetter
{
    
    // Get the graphics context.
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();
    
    // Put the text matrix into a known state. This ensures
    // that no old scaling factors are left in place.
    CGContextSetTextMatrix(currentContext, CGAffineTransformIdentity);
    
    // Create a path object to enclose the text. Use 72 point
    
    // margins all around the text.
    
    //CGRect    frameRect = CGRectMake(100, 100, 368, 648);
    CGRect    frameRect = CGRectMake(72, 72, 451.44, 697.68);
    
    CGMutablePathRef framePath = CGPathCreateMutable();
    
    CGPathAddRect(framePath, NULL, frameRect);
    
    // Get the frame that will do the rendering.
    
    // The currentRange variable specifies only the starting point. The framesetter
    
    // lays out as much text as will fit into the frame.
    
    CTFrameRef frameRef = CTFramesetterCreateFrame(framesetter, currentRange, framePath, NULL);
    
    CGPathRelease(framePath);
    
    // Core Text draws from the bottom-left corner up, so flip
    
    // the current transform prior to drawing.
    
    CGContextTranslateCTM(currentContext, 0, kDefaultPageHeight);
    
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    
    // Draw the frame.
    
    CTFrameDraw(frameRef, currentContext);
    
    // Update the current range based on what was drawn.
    
    currentRange = CTFrameGetVisibleStringRange(frameRef);
    
    currentRange.location += currentRange.length;
    
    currentRange.length = 0;
    
    CFRelease(frameRef);
    
    return currentRange;
    
}

- (void)drawPageNumber:(NSInteger)pageNum
{
    NSString* pageString = [NSString stringWithFormat:@"Page %ld", (long)pageNum];
    
    UIFont* theFont = [UIFont systemFontOfSize:25];
    
    CGSize pageStringSize = [pageString sizeWithAttributes:
                             
                             @{NSFontAttributeName:
                                   
                                   theFont}];
    
    CGRect stringRect = CGRectMake(((kDefaultPageWidth - pageStringSize.width) / 2.0),
                                   
                                   769.68 + ((72.0 - pageStringSize.height) / 2.0) ,
                                   
                                   pageStringSize.width,
                                   
                                   pageStringSize.height);
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15], NSParagraphStyleAttributeName: paragraphStyle};
    
    [pageString drawInRect:stringRect withAttributes:attributes];
    
}

- (IBAction)savePDFFile:(id)sender
{
    
    NSString* path = [[NSBundle mainBundle] pathForResource:@"sampleData" ofType:@"plist"];
    
    NSArray *arrayPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES);
    
    path = [arrayPaths objectAtIndex:0];
    
    pdfFilePath = [path stringByAppendingPathComponent:
                        
                        [NSString stringWithFormat:@"%@.pdf", @"Print_File"]];
    
    // Prepare the text using a Core Text Framesetter
    
    CFAttributedStringRef currentText = CFAttributedStringCreate(NULL,
                                                                 
                                                                 (CFStringRef)self.textView.text, NULL);
    
    CTFontRef font = CTFontCreateWithName((CFStringRef)@"Arial", 16.0f, nil);
    
    // CFAttributedStringSetAttribute(currentText,CFRangeMake(0, strLength-1), kCTFontAttributeName, font);
    
    NSInteger currentPage = 0;
    
    if (currentText) {
        
        CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(currentText);
        
        if (framesetter) {
            
            NSString* pdfFileName = pdfFilePath; //[NSString stringWithString:@"test.pdf"];
            
            // Create the PDF context using the default page: currently constants at the size
            
            // of 612 x 792.
            
            UIGraphicsBeginPDFContextToFile(pdfFileName, CGRectZero, nil);
            
            CFRange currentRange = CFRangeMake(0, 0);
            
            currentPage = 0;
            
            BOOL done = NO;
            
            do {
                
                // Mark the beginning of a new page.
                
                UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, kDefaultPageWidth,
                                                          
                                                          kDefaultPageHeight), nil);
                
                
                
                // Draw a page number at the bottom of each page
                
                currentPage++;
                
                //[self drawPageNumber:currentPage];
                
                
                // Render the current page and update the current range to
                
                // point to the beginning of the next page.
                
                currentRange = [self renderPage:currentPage withTextRange:
                                
                                currentRange andFramesetter:framesetter];
                
                
                
                // If we're at the end of the text, exit the loop.
                
                if (currentRange.location == CFAttributedStringGetLength
                    
                    ((CFAttributedStringRef)currentText))
                    
                    done = YES;
                
            } while (!done);
            
            
            // Close the PDF context and write the contents out.
            
            UIGraphicsEndPDFContext();
            
            // Release the framewetter.
            
            CFRelease(framesetter);
            
        } else {
            
            NSLog(@"Could not create the framesetter needed to lay out the atrributed string.");
            
        }
        
        // Release the attributed string.
        
        CFRelease(currentText);
        
    } else {
        
        NSLog(@"Could not create the attributed string for the framesetter");
        
    }
    
    currentPageNumber = currentPage;
//    if(print)
//    {
//        [self printDocument];
//    }
// Ask the user if they'd like to see the file or email it.
    
//    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:@"Would you like to preview or email this PDF?"
//                                  
//                                                             delegate:self
//                                  
//                                                    cancelButtonTitle:@"Cancel"
//                                  
//                                               destructiveButtonTitle:nil
//                                  
//                                                    otherButtonTitles:@"Preview", @"Email", nil];
    
    //[actionSheet showInView:self.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"Action Sheet button %ld", (long)buttonIndex);
    
    if (buttonIndex == 0) {
        
        // present a preview of this PDF File.
        
        QLPreviewController* preview = [[QLPreviewController alloc] init];
        
        preview.dataSource = self;
        
        [self presentModalViewController:preview animated:YES];
    }
    
    else if(buttonIndex == 1)
    {
        
        // email the PDF File.
        
        MFMailComposeViewController* mailComposer = [[MFMailComposeViewController alloc] init];
        
        mailComposer.mailComposeDelegate = self;
        
        [mailComposer addAttachmentData:[NSData dataWithContentsOfFile:pdfFilePath]
         
                               mimeType:@"application/pdf" fileName:@"report.pdf"];
        
        
        
        [self presentModalViewController:mailComposer animated:YES];
        
    }
}

- (void) printDocument{
    NSString* path = [[NSBundle mainBundle] pathForResource:@"sampleData" ofType:@"plist"];
    NSArray *arrayPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES);
    path = [arrayPaths objectAtIndex:0];
    pdfFilePath = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf", @"Print_File"]];
    
    UIPrintInteractionController *pc = [UIPrintInteractionController
                                        sharedPrintController];
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.orientation = UIPrintInfoOrientationPortrait;
    printInfo.jobName =@"Report";
    
    pc.printInfo = printInfo;
    pc.showsPageRange = YES;
    pc.printingItem = [NSURL fileURLWithPath:pdfFilePath];
    
    
    UIPrintInteractionCompletionHandler completionHandler =
    ^(UIPrintInteractionController *printController, BOOL completed,
      NSError *error) {
        if(!completed && error){
            NSLog(@"Print failed - domain: %@ error code %ld", error.domain,
                  (long)error.code);
        }
    };
    [pc presentFromRect:self.printButton.frame inView:self.view animated:YES completionHandler:completionHandler];
}

- (void) setPageNumber {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    self.pageNo.text = @"Updating...";
    dispatch_async(queue, ^{
        print = NO;
        [self savePDFFile:self];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.pageNo.text = [NSString stringWithFormat:@"Page: %ld", (long)currentPageNumber];
            if(print == YES)
            {
                [self printText:self];
            }
        });
    });
//    print = NO;
//    [self savePDFFile:self];
//    self.pageNo.text = [NSString stringWithFormat:@"%ld", (long)currentPageNumber];
}

#pragma mark - QLPreviewControllerDataSource

- (NSInteger) numberOfPreviewItemsInPreviewController: (QLPreviewController *) controller
{
    return 1;
}

- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index
{
    return [NSURL fileURLWithPath:pdfFilePath];
}

@end

//
//  ShowRecordingViewController.m
//  CallReply
//
//  Created by apple on 3/10/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "ShowRecordingViewController.h"
#import "RecordingViewController.h"
#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import "PlayAudioViewController.h"
#import "PlayerViewController.h"
#import "Audio.h"
#import "RecordinglTableViewCell.h"
//#import "SSZipArchive.h"
#import "ZipArchive.h"
#import "NSData+Encryption.h"
#import <MessageUI/MessageUI.h>
#import "SettingViewController.h"
#import "Constants.h"
#import "Utility.h"
#import "UserInfo.h"
#import "ExtAudioConverter.h"
#import <zlib.h>
#import "SubscriptionViewController.h"

//#import "Objective-Zip+NSError.h"
//#import "Objective-Zip.h"
//#import <objective-zip/crypt.h>
//#import "crypt.h"
@interface ShowRecordingViewController (){
    NSMutableArray *audios;
    Audio *audioToMailed;
    NSString *mailedFileName;
    CGRect keyboardFrame;
    UserInfo *userInfo;
    Audio *audioToRename;
}

@end
#define kisPasswordEnabled      @"isPasswordEnabled"
@implementation ShowRecordingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Speech To Text";
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorColor = [UIColor clearColor];
    self.cellArray = [[NSMutableArray alloc] init];
    //encryption code...
    NSString *key = @"mypassword";
    NSString *secret = @"text to encrypt";
    
    NSData *plain = [secret dataUsingEncoding:NSUTF8StringEncoding];
    NSData *cipher = [plain AES256EncryptWithKey:key];
    printf("%s\n", [[cipher description] UTF8String]);
    
    plain = [cipher AES256DecryptWithKey:key];
    printf("%s\n", [[plain description] UTF8String]);
    printf("%s\n", [[[NSString alloc] initWithData:plain encoding:NSUTF8StringEncoding] UTF8String]);
//    [NSString alloc] initWithData:[secret AES256DecryptWithKey:key] encoding:NSUTF8StringEncoding] autorelease];
    
//    [pool drain];
//    return 0;
    //end
    
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.title = @"Voice Recording";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Settings" style:UIBarButtonItemStylePlain target:self action:@selector(openSetting:)];
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).filePath = nil;
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).flag = false;
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).prevAudioPath = nil;
    userInfo = [UserInfo instance];
    userInfo.audioSessionForText = NO;
    [userInfo saveUserInfo];
    if(userInfo.audioSessionForVoice ){
            self.tabBarController.selectedIndex = 1;
        }
    userInfo.audioSessionForVoice = false;
    [userInfo saveUserInfo];
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).audio = nil;
    [self fetchAudios];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)startRecording:(id)sender {
    userInfo = [UserInfo instance];
    if(userInfo.isSubscribed || userInfo.numberOfFreeRecordings < knumberOfFreeRecordingsCount) {
        RecordingViewController *rvc = [[RecordingViewController alloc] initWithNibName:@"RecordingViewController" bundle:nil];
        // [self presentViewController:rvc animated:YES completion:nil];
        ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isNewRecording = YES;
        ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFileSaved = NO;
        ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isUpdated = NO;
        [self.navigationController pushViewController:rvc animated:YES];
    } else {
        [self askUserForSubscription];
    }
}

- (void) startRecording {
    
}
#pragma mark - SWTableViewDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            NSLog(@"check button was pressed");
            break;
        case 1:
            NSLog(@"clock button was pressed");
            break;
        case 2:
            NSLog(@"cross button was pressed");
            break;
        case 3:
            NSLog(@"list button was pressed");
        default:
            break;
    }
}
#pragma mark - Swipeable TableViewCell Delegate functions

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    NSInteger tag = cell.tag-10000;
    Audio *audio = (Audio*)[audios objectAtIndex:tag];
  //  NSString*filePath = audio.filePath;
    NSURL* url = [NSURL URLWithString:audio.filePath];
    if (index == 1) {
        [self removeFile:url];
    } else {
        audioToRename = audio;
        [self changeName];
    }
}


- (void)changeName {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Update File Name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
    alert.tag = 10001;
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    UITextField * alertTextField = [alert textFieldAtIndex:0];
    alertTextField.placeholder = @"";
    [alert show];
    
}

- (void)removeFile:(NSURL*)url{
    NSString *path = [url absoluteString];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    BOOL success = [fileManager removeItemAtPath:[url path] error:&error];
    if (success) {
        [self deleteAudio:path];
        NSLog(@"File deleted");
                UIAlertView *removedSuccessFullyAlert = [[UIAlertView alloc] initWithTitle:@"Congratulations:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [removedSuccessFullyAlert show];
    } else{
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:78.0/255.0f green:154.0/255.0 blue:212.0/255.0 alpha:1.0]
                                                title:@"Rename"];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.9960f green:0.247f blue:0.207 alpha:1.0f]
                                                title:@"Trash"];
    return rightUtilityButtons;
}


#pragma mark - TableViewCell Delegate Functions

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [audios count];    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Audio *audio = (Audio*)[audios objectAtIndex:indexPath.row];
    static NSString *MyIdentifier = @"recordingCell";
    RecordinglTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RecordinglTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.rightUtilityButtons = [self rightButtons];
       // [cell setSeparatorInset:UIEdgeInsetsZero];
        [cell setDelegate:self];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = 10000+indexPath.row;
    [cell.shareAudio addTarget:self action:@selector(sendEmailButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    NSArray* fileNameArr = [audio.name componentsSeparatedByString: @"_"];
    cell.shareAudio.tag = 10000+indexPath.row;
    NSInteger count = indexPath.row + 1;
    NSString* str ;
    if([audio.isNameChanged isEqualToString:@"NO"])
        str = [NSString stringWithFormat:@"%@_%@_%@_%ld",[fileNameArr objectAtIndex:0] ,[fileNameArr objectAtIndex:1],[fileNameArr objectAtIndex:2], (long)count];
    else
        str = audio.name;
    cell.audiofileName.text = str;
    cell.contentView.tag = 10000+indexPath.row;
    UITapGestureRecognizer *cellTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openAudio:)];
    [cell.contentView addGestureRecognizer:cellTapRecognizer];
    if(![self.cellArray containsObject:cell])
        [self.cellArray insertObject:str atIndex:indexPath.row];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"cell selected");
}

-(void) openAudio:(UITapGestureRecognizer*)sender {
    userInfo = [UserInfo instance];
    if(userInfo.isSubscribed || userInfo.numberOfFreeRecordings < knumberOfFreeRecordingsCount) {
        userInfo = [UserInfo instance];
        userInfo.audioSessionForVoice = YES;
        [userInfo saveUserInfo];
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        appDelegate.isNewRecording = NO;
        Audio *audio = (Audio*)[audios objectAtIndex:sender.view.tag-10000];
        RecordingViewController *rvc = [[RecordingViewController alloc] initWithNibName:@"RecordingViewController" bundle:nil];
        PlayerViewController *pvc = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController" bundle:nil];
        pvc.url = [NSURL URLWithString:[audio.filePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        ((AppDelegate*)[[UIApplication sharedApplication] delegate]).audio = audio;
        ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFileSaved = YES;
        ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isNewRecording = NO;
        ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isUpdated = NO;
        
        NSMutableArray *controllers = [self.navigationController.viewControllers mutableCopy];
        [controllers addObject:rvc];
        [controllers addObject:pvc];
        [self.navigationController setViewControllers:controllers animated:YES];
    } else {
        [self askUserForSubscription];
    }
}

- (void)askUserForSubscription {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Subscription"
                                 message:@"Please subscribe to use full features of \"Dragon Speech Recording\""
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* subscribe = [UIAlertAction
                                actionWithTitle:@"Subscribe"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    SubscriptionViewController *subscriptionViewController = [[SubscriptionViewController alloc] init];
                                    [self.navigationController pushViewController:subscriptionViewController animated:YES];
//                                    [((AppDelegate*)[[UIApplication sharedApplication] delegate]) getSubscribed];
                                }];
    UIAlertAction* retore = [UIAlertAction
                             actionWithTitle:@"Restore"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 [((AppDelegate*)[[UIApplication sharedApplication] delegate]) restoreSubscription];
                             }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:nil];
    [alert addAction:subscribe];
    [alert addAction:retore];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) shareAudioWithActivityController
{
    
}

#pragma mark - Email Controller delegate
//This is old one...

//-(void)sendEmailButtonClicked:(UIButton*)sender{
//    
//    NSInteger tag = sender.tag - 10000;
//    Audio *audio = (Audio*)[audios objectAtIndex:tag];
//    NSString *emailTitle = @"Audio Recording";
//    NSString *messageBody = @"";
//    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
//    if ([MFMailComposeViewController canSendMail]){
//        mc.mailComposeDelegate = self;
//        [mc setSubject:emailTitle];
//        [mc setMessageBody:messageBody isHTML:NO];
//        NSString* filePath = [NSString stringWithFormat:@"%@", audio.filePath];
//        NSURL *url = [NSURL fileURLWithPath:filePath];
//        NSData *fileData = [NSData dataWithContentsOfURL:url];
//        
//        NSURL* fileURL = [NSURL URLWithString:filePath];
//
//        
//        NSURLRequest* fileUrlRequest = [[NSURLRequest alloc] initWithURL:fileURL cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:.1];
//        
//        NSError* error = nil;
//        NSURLResponse* response = nil;
//        NSData* fData = [NSURLConnection sendSynchronousRequest:fileUrlRequest returningResponse:&response error:&error];
//        NSString* mimeType = [response MIMEType];
//         NSData* data = [NSData dataWithContentsOfURL:fileURL options:0 error:&error];
//        
//        NSLog(@"%@", mimeType);
//        if(data != nil){
//            [mc addAttachmentData:data mimeType:@"audio/mp4" fileName:@"tag"];
//        }
//        if (error) {
//            NSLog(@"Unable to load file from provided URL %@: %@", filePath, error);
//        }
//        [self presentViewController:mc animated:YES completion:NULL];
//    }
//    else{
//        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Fail" message:@"You cannot send email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        
//        [alert show];
//    }
//    
//    NSLog(@"hello hi");
//}

//This is new one...

#pragma mark - Email Sending controller
-(void)sendEmailButtonClicked:(UIButton*)sender{
    NSString *enablePassword = [[NSUserDefaults standardUserDefaults]
                                stringForKey:kisPasswordEnabled];
    NSInteger tag = sender.tag - 10000;
    mailedFileName = [self.cellArray objectAtIndex:tag];
    audioToMailed = (Audio*)[audios objectAtIndex:tag];
    NSString *emailTitle = @"Audio Recording";
    NSString *messageBody = @"";
    NSString *emailAddress = @"";
    //BOOL converted = [Utility convertAudio:audioToMailed.filePath];
    BOOL textCreated = [Utility createTextFile:audioToMailed.filePath];
    
    ExtAudioConverter* converter = [[ExtAudioConverter alloc] init];
    converter.inputFile =  audioToMailed.filePath;
    NSString* outputpath = [audioToMailed.filePath stringByReplacingOccurrencesOfString:@".m4a" withString:@".wav"];
    converter.outputFile = outputpath;
    converter.outputSampleRate = 22050;
    converter.outputNumberChannels = 2;
    converter.outputBitDepth = BitDepth_16;
    converter.outputFormatID = kAudioFormatLinearPCM;
    converter.outputFileType = kAudioFileWAVEType;
    [converter convert];
    
    if([enablePassword isEqualToString:@"no"]){
        [self sendMailWithoutPassword];
    }else{
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter password for audio file" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;

        UITextField * alertTextField = [alert textFieldAtIndex:0];
        alertTextField.keyboardType = UIKeyboardTypeNumberPad;
        alertTextField.placeholder = @"";
        alert.tag = 101;
        [alert show];
    }
    
//MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];

//    if ([MFMailComposeViewController canSendMail]){
////        mc.mailComposeDelegate = self;
//        [mc setMailComposeDelegate:self];
//        [mc setSubject:emailTitle];
//        [mc setMessageBody:messageBody isHTML:NO];
//        //[mc setToRecipients:[NSArray arrayWithObject:emailAddress]];
//        NSString *filePath = [NSString stringWithFormat:@"%@_c.wav",[audioToMailed.filePath stringByDeletingPathExtension]];
//
//      //  NSURL *url = [NSURL fileURLWithPath:filePath];
//      //  NSData *fileData = [NSData dataWithContentsOfURL:url];
//
//        NSURL* fileURL = [NSURL URLWithString:filePath];
//        NSData *data = [NSData dataWithContentsOfURL:fileURL options:0 error:nil];
//
//
//        if([enablePassword isEqualToString:@"no"]){
////            [mc addAttachmentData:data mimeType:@"audio/wav" fileName:[NSString stringWithFormat:@"%@.wav",mailedFileName]];
////            [self presentViewController:mc animated:YES completion:NULL];
//            [self sendMailWithoutPassword];
//
//        }else{
//
//            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter password for audio file" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
//            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
//
//            UITextField * alertTextField = [alert textFieldAtIndex:0];
//            alertTextField.keyboardType = UIKeyboardTypeNumberPad;
//            alertTextField.placeholder = @"";
//            alert.tag = 101;
//            [alert show];
//        }
//    }
//    else{
//        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Fail" message:@"You cannot send email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//    }
    
}


-(IBAction)shareButtonClicked:(id)sender{
    NSLog(@"hello world");
    
//    NSInteger tag =
    
    // Email Subject
    NSString *emailTitle = @"Test Email";
    // Email Content
    NSString *messageBody = @"Test Subject!";
    // To address
//    NSArray *toRecipents = [NSArray arrayWithObject:@"support@test.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]){
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        
//        [mc addAttachmentData:data mimeType:@"audio/mpeg-4" fileName:filename];
        
    //    [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    else{
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Fail" message:@"You cannot send email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            [self removeXmlAndWaveFile];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            [self removeXmlAndWaveFile];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self removeXmlAndWaveFile];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [self removeXmlAndWaveFile];
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)removeXmlAndWaveFile {
    NSString* wavFilepath = [audioToMailed.filePath stringByReplacingOccurrencesOfString:@".m4a" withString:@".wav"];
    NSURL* wavFileUrl = [NSURL URLWithString:wavFilepath];
    
    NSString* xmlFilePath = [audioToMailed.filePath stringByReplacingOccurrencesOfString:@".m4a" withString:@".wav.xml"];
    NSURL* xmlFileUrl = [NSURL URLWithString:xmlFilePath];
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtURL:wavFileUrl error:&error];
    NSLog(@"ERROR deleting wav file: %@", error);
    [[NSFileManager defaultManager] removeItemAtURL:xmlFileUrl error:&error];
    NSLog(@"ERROR deleting xml file: %@", error);
    
}

-(void)fetchAudios{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Audio" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSError *error;
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"time" ascending:NO];

    audios = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:request error:&error]];
    audios = [[NSMutableArray alloc]initWithArray:[audios sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]]];
    [self.tableView reloadData];
}

- (void)deleteAudio:(NSString*)url{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *audioEntity=[NSEntityDescription entityForName:@"Audio" inManagedObjectContext:context];
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:audioEntity];
    NSPredicate *p=[NSPredicate predicateWithFormat:@"filePath == %@", url];
    [fetch setPredicate:p];
    NSError *fetchError;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *audio in fetchedProducts) {
        [context deleteObject:audio];
        NSError *error = nil;
        if (![context save:&error])
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self fetchAudios];
    });

}

- (void)handleBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)openSetting:(id)sender{
    //if([[NSUserDefaults standardUserDefaults] boolForKey:kuserIsSubscribed]) {
        SettingViewController *svc = [[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
        [self.navigationController pushViewController:svc animated:YES];
  //  } else {
    //    [self askUserForSubscription];
   // }
}

- (void)sendMailWithoutPassword {
    
    NSString* password = @"";
    
    NSString *emailTitle = @"Audio Recording";
    //NSString *messageBody = [NSString stringWithFormat:@"Password: %@",password];
    NSString *messageBody = @"";
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    UIActivityViewController *activityViewController;
    
    //        mc.mailComposeDelegate = self;
    [mc setMailComposeDelegate:self];
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    // [mc setToRecipients:[NSArray arrayWithObject:emailAddress]];
    NSString* filePath = [NSString stringWithFormat:@"%@", audioToMailed.filePath];
    NSURL *url = [NSURL fileURLWithPath:filePath];
    NSData *fileData = [NSData dataWithContentsOfURL:url];
    NSURL* fileURL = [NSURL URLWithString:audioToMailed.filePath];
    NSError* error = nil;
    NSURLResponse* response = nil;
    NSData* data = [NSData dataWithContentsOfURL:fileURL options:0 error:&error];
    
    if(data != nil){
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Logger.zip"]];
        ZipArchive *zipFile = [[ZipArchive alloc] init];
        
        [zipFile CreateZipFile2:zipFilePath Password:password];
        NSString *filePath = [NSString stringWithFormat:@"%@_c.wav",[audioToMailed.filePath stringByDeletingPathExtension]];
        NSString* outputpath = [audioToMailed.filePath stringByReplacingOccurrencesOfString:@".m4a" withString:@".wav"];
        NSString* xmlFilePath = [outputpath stringByAppendingString:@".xml"];
        [zipFile addFileToZip: xmlFilePath newname:[NSString stringWithFormat:@"%@.wav.xml",mailedFileName]];
        [zipFile addFileToZip: outputpath newname:[NSString stringWithFormat:@"%@.wav",mailedFileName]];
        [zipFile CloseZipFile2];
        NSData* d = [NSData dataWithContentsOfFile:zipFilePath];
        NSURL *zipFileUrl    = [NSURL fileURLWithPath:zipFilePath];
        activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[@"Audio Recording", zipFileUrl] applicationActivities:nil];
        //[mc addAttachmentData:d mimeType:@"application/zip" fileName:[NSString stringWithFormat:@"%@.zip",mailedFileName]];
    }
    if (error) {
        NSLog(@"Unable to load file from provided URL %@: %@", filePath, error);
    }
    [self presentViewController:activityViewController animated:YES completion:nil];
}

#pragma mark - AlertView Delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
   
    if(alertView.tag == 101){

    NSString *password   = [alertView textFieldAtIndex:0].text;
        if(![password isEqualToString:@""]){
        NSString *emailTitle = @"Audio Recording";
        //NSString *messageBody = [NSString stringWithFormat:@"Password: %@",password];
        NSString *messageBody = @"";

        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        UIActivityViewController *activityViewController;
        
            //        mc.mailComposeDelegate = self;
            [mc setMailComposeDelegate:self];
            [mc setSubject:emailTitle];
            [mc setMessageBody:messageBody isHTML:NO];
           // [mc setToRecipients:[NSArray arrayWithObject:emailAddress]];
            NSString* filePath = [NSString stringWithFormat:@"%@", audioToMailed.filePath];
            NSURL *url = [NSURL fileURLWithPath:filePath];
            NSData *fileData = [NSData dataWithContentsOfURL:url];
            NSURL* fileURL = [NSURL URLWithString:audioToMailed.filePath];
                NSError* error = nil;
                NSURLResponse* response = nil;
                NSData* data = [NSData dataWithContentsOfURL:fileURL options:0 error:&error];
                
                if(data != nil){
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString* zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Logger.zip"]];
                    ZipArchive *zipFile = [[ZipArchive alloc] init];
                    NSLog(@"Password: %@",password);
                    [zipFile CreateZipFile2:zipFilePath Password:password];
                    NSString *filePath = [NSString stringWithFormat:@"%@_c.wav",[audioToMailed.filePath stringByDeletingPathExtension]];
                    NSString* outputpath = [audioToMailed.filePath stringByReplacingOccurrencesOfString:@".m4a" withString:@".wav"];
                    NSString* xmlFilePath = [outputpath stringByAppendingString:@".xml"];
                    [zipFile addFileToZip: xmlFilePath newname:[NSString stringWithFormat:@"%@.wav.xml",mailedFileName]];
                    [zipFile addFileToZip: outputpath newname:[NSString stringWithFormat:@"%@.wav",mailedFileName]];
                    [zipFile CloseZipFile2];
                    NSData* d = [NSData dataWithContentsOfFile:zipFilePath];
                    //[mc addAttachmentData:d mimeType:@"application/zip" fileName:[NSString stringWithFormat:@"%@.zip",mailedFileName]];
                    NSURL *zipFileUrl    = [NSURL fileURLWithPath:zipFilePath];
                    activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[@"Audio Recording", zipFileUrl] applicationActivities:nil];
                }
                if (error) {
                    NSLog(@"Unable to load file from provided URL %@: %@", filePath, error);
                }
                //[self presentViewController:mc animated:YES completion:NULL];
                [self presentViewController:activityViewController animated:YES completion:nil];
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Password is Missing"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    } else if (alertView.tag == 10001) {
        if(buttonIndex == 1){
            NSString *fileName   = [alertView textFieldAtIndex:0].text;
            if([fileName length] > 0){
                AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                NSManagedObjectContext *context = [appDelegate managedObjectContext];
                NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Audio" inManagedObjectContext:context];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"filePath == %@", audioToRename.filePath];
                NSFetchRequest *request = [[NSFetchRequest alloc] init];
                [request setEntity:entityDesc];
                [request setPredicate:predicate];
                NSError *error;
                NSMutableArray *audioArr = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:request error:&error]];
                Audio* audio = [audioArr objectAtIndex:0];
                audio.name = fileName;
                audio.isNameChanged = @"YES";
                if (![context save:&error])
                    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self fetchAudios];
                });
            }
        }
        
    }
    
}

@end

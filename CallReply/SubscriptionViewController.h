//
//  SubscriptionViewController.h
//  TranscribeForMe
//
//  Created by Badar Iqbal on 16/04/2018.
//  Copyright © 2018 tbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubscriptionViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *scrollViewContainer;
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UIButton *privacyPolicyButton;
@property (strong, nonatomic) IBOutlet UIButton *termsOfServiceButton;
@property (strong, nonatomic) IBOutlet UIButton *subscribeButton;

@end

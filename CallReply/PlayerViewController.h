//
//  PlayerViewController.h
//  CallReply
//
//  Created by apple on 3/10/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Audio.h"

@interface PlayerViewController : UIViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *play;
@property (weak, nonatomic) IBOutlet UIButton *stop;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIButton *overload;
@property (weak, nonatomic) IBOutlet UIButton *save;
@property (weak, nonatomic) IBOutlet UILabel *processing;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (weak, nonatomic) IBOutlet UIButton *insert;
@property  BOOL isNewRecording;
@property (weak, nonatomic) Audio *audio;

@property BOOL flag;
@property BOOL isMergedRemainingAudio;
@property BOOL isFileSaved;
@property NSURL *url;
@property NSURL *Remainingurl;
@property NSURL *prevAudioUrl;
- (IBAction)saveAudio:(id)sender;
- (IBAction)stopAudio:(id)sender;
- (IBAction)playAudio:(id)sender;
- (IBAction)overloadAction:(id)sender;
- (IBAction)insertAudio:(id)sender;


@end

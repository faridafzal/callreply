//
//  Audio.h
//  CallReply
//
//  Created by apple on 3/14/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Audio : NSManagedObject
@property (nonatomic, strong) NSDate * time;
@property (nonatomic, strong) NSString * filePath;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString *isNameChanged;
@end

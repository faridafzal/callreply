//
//  HomeViewController.h
//  CallReply
//
//  Created by apple on 3/10/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"


@interface HomeViewController : UIViewController <SWTableViewCellDelegate, UITableViewDataSource, UITableViewDelegate>
- (IBAction)showRecordings:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *voiceRecordingBtn;
@property (weak, nonatomic) NSString *titleString;
@property (weak, nonatomic) IBOutlet UIButton *speechToTextBtn;
- (IBAction)sppechToTextBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *cellArray;


@end

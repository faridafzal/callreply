//
//  Text.m
//  CallReply
//
//  Created by apple on 10/17/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "Text.h"

@implementation Text
@dynamic text;
@dynamic time;
@dynamic name;
@dynamic isNameChanged;
#pragma mark Hooks
- (void)willInsertObject
{
    // If you extend XMPPMessageArchiving_Contact_CoreDataObject,
    // you can override this method to use as a hook to set your own custom properties.
}

- (void)didUpdateObject
{
    // If you extend XMPPMessageArchiving_Contact_CoreDataObject,
    // you can override this method to use as a hook to update your own custom properties.
}
@end

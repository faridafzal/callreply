//
//  PlayAudioViewController.m
//  CallReply
//
//  Created by apple on 3/14/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "PlayAudioViewController.h"
#import <CoreData/CoreData.h>
#import <AudioToolbox/AudioToolbox.h>
#import "Toast+UIView.h"
#import "AppDelegate.h"

@interface PlayAudioViewController (){
   // AVAudioPlayer *player;
    float ticks;
}

@end

@implementation PlayAudioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Voice Recording";
    self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backBtn"] style:UIBarButtonItemStylePlain target:self action:@selector(handleBack:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(changeName:)];


        NSString *escapedUrlString = [_audio.filePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *aUrl = [NSURL URLWithString:escapedUrlString];
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
    NSLog(@"%@",[aUrl absoluteString]);
    _player = [[AVAudioPlayer alloc]
              initWithContentsOfURL:aUrl error:nil];
    [_player setVolume:1.0];

    self.slider.minimumValue = 0;
    self.slider.maximumValue = 100;
    self.slider.continuous = true;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    ticks =0;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_player stop];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)playAudio:(id)sender {
    [_player play];
     [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSeekBar) userInfo:nil repeats:YES];
}
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    NSLog(@"delegate called");
    _player = nil;
}
- (IBAction)stopAudio:(id)sender {
    [_player stop];
}

- (void)handleBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)changeName:(id)sender{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Update File Name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    UITextField * alertTextField = [alert textFieldAtIndex:0];
    alertTextField.placeholder = @"";
    [alert show];

}

- (void)updateSeekBar{
    float progress = _player.currentTime;
    float tt = _player.duration;
    float p = (progress/tt)*100;
   // self.time.text =  [NSString stringWithFormat:@"%.02f",progress/60];
    [self.slider setValue:p];
    
    double hours  = _player.currentTime / 3600 ;
    double minutes = fmod(trunc(_player.currentTime / 60.0), 60.0);
    double seconds = fmodf(_player.currentTime,60.0f);
    self.time.text = [NSString stringWithFormat: @"%02.f:%02.f:%02.f", hours, minutes, seconds];
   // self.rlbl.text = [NSString stringWithFormat:@"%02.0f:%02.0f", minutes, seconds];
}

#pragma mark-alertview delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1){
        NSString *fileName   = [alertView textFieldAtIndex:0].text;
        if([fileName length] > 0){
            AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Audio" inManagedObjectContext:context];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"filePath == %@", self.audio.filePath];
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            [request setEntity:entityDesc];
            [request setPredicate:predicate];
            NSError *error;
            NSMutableArray *audios = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:request error:&error]];
            Audio* audio = [audios objectAtIndex:0];
            audio.name = fileName;
            audio.isNameChanged = @"YES";
            if (![context save:&error])
                    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
}
@end

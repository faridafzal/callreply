//
//  RecordingViewController.m
//  CallReply
//
//  Created by apple on 3/10/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "RecordingViewController.h"
#import "PlayerViewController.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "UserInfo.h"


@interface RecordingViewController (){
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    NSTimer *_timer;
    float ticks;
    UserInfo *userInfo;
;
}
@end

@implementation RecordingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    // Do any additional setup after loading the view from its nib.
    //self.stopAudio.hidden = YES;
    
    //These lines are used to show timer.
//    float progress = player.currentTime;
//    self.time.text = [NSString stringWithFormat:@"%.02f",progress/60];
    
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadAlertView)
                                                 name:@"ExpireSession"
                                               object:nil];

        ticks = 0;
    self.rlbl.text = @"";
    self.title = @"Voice Recording";
    self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backBtn"] style:UIBarButtonItemStylePlain target:self action:@selector(handleBack:)];

    self.rlbl.hidden = YES;
    self.rbtn.hidden = YES;
    self.activityInd.hidden = YES;
    [self.recordBtn setTitle:@"" forState:UIControlStateNormal];
    UIImage *closebtnimg = [UIImage imageNamed:@"rec1"];
    [self.recordBtn setImage: closebtnimg forState:UIControlStateNormal];
    self.rlbl.hidden = NO;

    
    userInfo = [UserInfo instance];
    userInfo.audioSessionForText = NO;
    [userInfo saveUserInfo];
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"MyAudioMemo.m4a",
                               nil];
    
    NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"/" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@":" withString:@""];
    dateString = [NSString stringWithFormat:@"%@%@",@"UR_VOICE_", dateString];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:dateString];
    fullPath = [fullPath stringByAppendingFormat:@"_n"];
    fullPath = [fullPath stringByAppendingFormat:@".m4a"];
    NSURL *outputFileURL  = [NSURL fileURLWithPath:fullPath];
    (NSLog(@"Prev %@",[outputFileURL absoluteString]));
      //NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    //[session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error: nil];
    [session setMode:AVAudioSessionModeDefault error:nil];
    
    //   Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
   [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
   //[recordSetting setValue:[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];

    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    recorder = nil;
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
    AppDelegate *delegate = [self appDelegate];
    if(delegate.flag){
        [self recordAudio:self];
    } 
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
     AppDelegate *delegate = [self appDelegate];
    delegate.flag = false;
    if(_timer){
        [_timer invalidate];
        _timer = nil;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)recordAudio:(id)sender {    
//    if (player.playing) {
//        [player stop];
//    }
    userInfo = [UserInfo instance];
    userInfo.audioSessionForVoice = YES;
    [userInfo saveUserInfo];
    if (!recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        [recorder record];
     //   [self.recordBtn setTitle:@"Stop" forState:UIControlStateNormal];
        UIImage *closebtnimg = [UIImage imageNamed:@"rec"];
        [self.recordBtn setImage: closebtnimg forState:UIControlStateNormal];
        self.rlbl.hidden = NO;
        
        
        self.rlbl.text = [NSString stringWithFormat:@"00:00:00"];
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
//        float progress = recorder.currentTime;
//        self.rlbl.text = [NSString stringWithFormat:@"%.02f",progress/60];
        
        self.rbtn.hidden = NO;
        self.activityInd.hidden = NO;
        self.rbtn.alpha = 1.0f;
        // Then fades it away after 2 seconds (the cross-fade animation will take 0.5s)
        [UIView animateWithDuration:0.3 delay:2.0 options:0 animations:^{
            // Animate the alpha value of your imageView from 1.0 to 0.0 here
            self.rbtn.alpha = 0.0f;
        } completion:^(BOOL finished) {
            // Once the animation is completed and the alpha has gone to 0.0, hide the view for good
            self.rbtn.hidden = YES;
        }];
        [self.activityInd startAnimating];

    
    } else {
        
        if(_timer){
            [_timer invalidate];
            _timer = nil;
        }
        [recorder stop];
        [self.activityInd stopAnimating];
        self.activityInd.hidden = YES;
        self.rlbl.hidden = YES;
        AppDelegate *delegate = [self appDelegate];
        delegate.flag = false;
        PlayerViewController *pvc = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController" bundle:nil];
        pvc.url = recorder.url;
        pvc.isFileSaved = NO;
        NSLog(@"pvc %@", [pvc.url absoluteString]);
        [self.navigationController pushViewController:pvc animated:YES];
    }
}
//if (!recorder.recording){
//    player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
//    [player setDelegate:self];
//    [player play];
//}

- (void)timerTick:(NSTimer *)timer
{
    // Timers are not guaranteed to tick at the nominal rate specified, so this isn't technically accurate.
    // However, this is just an example to demonstrate how to stop some ongoing activity, so we can live with that inaccuracy.
    
    ticks += 1;
    double hours  = ticks / 3600 ;
    double seconds = fmod(ticks, 60.0);
   // double minutes = fmod(trunc(ticks / 60.0), 60.0);
    double minutes = fmod(trunc(ticks / 60.0), 60.0);

    self.rlbl.text = [NSString stringWithFormat: @"%02.f:%02.f:%02.f", hours, minutes, seconds];

}

- (IBAction)stopAudio:(id)sender {
    
    //if (!recorder.recording){
//        NSString *urk = recorder.url;
//        player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
//        [player setDelegate:self];
//        [player play];
//        PlayerViewController *pvc = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController" bundle:nil];
//        pvc.url = recorder.url;
      //  [self presentViewController:pvc animated:YES completion:nil];

  //  }
//    [self.activityInd stopAnimating];
//    [recorder stop];
//    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
//    [audioSession setActive:NO error:nil];
//    PlayerViewController *pvc = [[PlayerViewController alloc] initWithNibName:@"PlayerViewController" bundle:nil];
//    pvc.url = recorder.url;
//    [self presentViewController:pvc animated:YES completion:nil];
}

-(AppDelegate*)appDelegate{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}

- (void)handleBack:(id)sender{
    dispatch_async(dispatch_get_main_queue(), ^{
        userInfo = [UserInfo instance];
        userInfo.audioSessionForVoice = NO;
        [userInfo saveUserInfo];
        [self.navigationController popViewControllerAnimated:YES];
    });
}

#pragma mark -AVAudioRecorderDelegate
- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    [self.activityInd stopAnimating];
    [recorder stop];
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}

- (void) loadAlertView {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:@"By doing this your current recording will become empty"
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    [self.navigationController popViewControllerAnimated:YES];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
}


@end

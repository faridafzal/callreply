//
//  Text.h
//  CallReply
//
//  Created by apple on 10/17/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Text : NSManagedObject
@property (nonatomic, strong) NSDate * time;
@property (nonatomic, strong) NSString * text;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString *isNameChanged;
@end

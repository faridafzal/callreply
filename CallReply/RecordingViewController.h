//
//  RecordingViewController.h
//  CallReply
//
//  Created by apple on 3/10/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface RecordingViewController : UIViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *recordBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;
@property (weak, nonatomic) IBOutlet UILabel *rlbl;
@property (weak, nonatomic) IBOutlet UIButton *stopAudio;
@property (weak, nonatomic) IBOutlet UIButton *rbtn;
- (IBAction)stopAudio:(id)sender;
- (IBAction)recordAudio:(id)sender;
@end

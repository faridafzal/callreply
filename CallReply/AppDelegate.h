//
//  AppDelegate.h
//  CallReply
//
//  Created by apple on 3/10/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Audio.h"
#import "ShowRecordingViewController.h"
#import "SpeechTextViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) Audio *audio;
@property (strong, atomic) NSArray *products;

@property NSURL *filePath;
@property NSURL *prevAudioPath;
@property double audioDuration;
@property BOOL isInsert;
@property float prevEndMarker;
//@property BOOL audioSessionForText;
//@property BOOL audioSessionForVoice;
@property BOOL isNewRecording;
@property BOOL isFileSaved;
@property BOOL isUpdated;
@property BOOL flag;
- (void)hideToastActivity;
- (void)getSubscribed;
- (void)restoreSubscription;


@end


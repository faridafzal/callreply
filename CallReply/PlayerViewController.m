//
//  PlayerViewController.m
//  CallReply
//
//  Created by apple on 3/10/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "PlayerViewController.h"
#import <CoreData/CoreData.h>
#import <AudioToolbox/AudioToolbox.h>
#import "RecordingViewController.h"
#import "AppDelegate.h"
#import "UserInfo.h"
#import "Audio.h"
#import "Toast+UIView.h"
#import "Constants.h"

@interface PlayerViewController (){
    AVAudioPlayer *player;
    NSTimer *timer;
    float currTime;
    bool trimFlag;
    int tapCount;
    bool isSaved;
    float ticks;
    UserInfo *userInfo;
}

@end

@implementation PlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Voice Recording";
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonSystemItemBACK target:self action:@selector(handleBack:)];
    self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backBtn"] style:UIBarButtonItemStylePlain target:self action:@selector(handleBack:)];
    //self.navigationItem.leftBarButtonItem.image = [UIImage imageNamed:@"backBtn"];
    _isMergedRemainingAudio = NO;
    self.processing.hidden = YES;
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadAlertView)
                                                 name:@"ExpireSession"
                                               object:nil];
    

    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    delegate.flag = false;
    self.isNewRecording = delegate.isNewRecording;
    self.audio = delegate.audio;
    if(delegate.prevAudioPath != nil){
        self.overload.userInteractionEnabled = NO;
        self.insert.userInteractionEnabled = NO;
        self.slider.userInteractionEnabled = NO;
        self.play.userInteractionEnabled = NO;
        self.stop.userInteractionEnabled = NO;
        if(delegate.filePath == nil ){
            // [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(mergeTwoAudioFiles) userInfo:nil repeats:NO];
            delegate.filePath = self.url;
            self.url = delegate.prevAudioPath;
            _isMergedRemainingAudio = YES;

        }
        NSLog(@"delegate %@",[delegate.filePath absoluteString]);
        NSLog(@"self %@",[_url absoluteString]);
        self.processing.hidden = NO;

        [self.view makeToastActivity];
        [self mergeTwoAudioFiles];
    }
    else{
        player = [[AVAudioPlayer alloc]
                  initWithContentsOfURL:self.url error:nil];
        [player setVolume: 1.0];
    }
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sliderTapped:)];
    [self.slider addGestureRecognizer:tapGestureRecognizer];
    
    // AVURLAsset* audioAsset = [AVURLAsset URLAssetWithURL:_url options:nil];
    // CMTime audioDuration = audioAsset.duration;
    self.slider.minimumValue = 0;
    self.slider.maximumValue = 100;
    self.slider.continuous = YES;
    trimFlag = false;
    [self.slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    userInfo = [UserInfo instance];
    userInfo.audioSessionForText = NO;
    userInfo.audioSessionForVoice = YES;

    [userInfo saveUserInfo];
//    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(handleBack:)];
//    self.navigationController.navigationBar.translucent = NO;
    ticks = 0;
    _flag = NO;
    isSaved = false;
    tapCount = 0;
    self.title = @"Voice Recording";
  //  AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    if(delegate.isFileSaved){
//        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action: @selector(changeName:)];
//    }
    trimFlag = false;

}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if([player isPlaying]){
        [player stop];

    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - action methods

- (IBAction)sliderValueChanged:(UISlider *)sender {
    bool a = false;
    if([player isPlaying]){
       // [player stop];
        a = true;
    }
    else
        [self invalidateTimer];
    NSLog(@"slider value = %f", sender.value);
    player.currentTime = (sender.value/100)*player.duration;
    currTime = player.currentTime;
    float progress = player.currentTime;
   // self.time.text = [NSString stringWithFormat:@"%.02f",progress/60];
    double hours  = player.currentTime / 3600 ;
    double minutes = fmod(trunc(player.currentTime / 60.0), 60.0);
    double seconds = fmodf(player.currentTime,60.0f);
    self.time.text = [NSString stringWithFormat: @"%02.f:%02.f:%02.f", hours, minutes, seconds];
    [self.view setNeedsDisplay];
    if(a)
        [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(playAudio:) userInfo:nil repeats:NO];
}
- (void)deleteAudio {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *audioEntity=[NSEntityDescription entityForName:@"Audio" inManagedObjectContext:context];
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:audioEntity];
    NSPredicate *p=[NSPredicate predicateWithFormat:@"time == %@", self.audio.time];
    [fetch setPredicate:p];
    NSError *fetchError;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *audio in fetchedProducts) {
        [context deleteObject:audio];
        NSError *error = nil;
        if (![context save:&error]){
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            
        }
    }
    NSString *path = self.audio.filePath;
    if(appDelegate.isUpdated){
        appDelegate.isFileSaved = NO;
        appDelegate.audio = nil;
    }
    [self remove:path];
}
- (void)saveAudioInDb {
    _flag = NO;
    isSaved = true;
    AppDelegate *delegate = [self appDelegate];
    delegate.filePath = nil;
    NSDateFormatter *formatter;
    NSString *dateString;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    //[formatter setDateFormat:@"dd/MM/yyyy"];
    dateString = [formatter stringFromDate:[NSDate date]];
    NSDate *time = [formatter dateFromString:dateString];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"/" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@":" withString:@""];
    dateString = [NSString stringWithFormat:@"%@%@",@"UR_VOICE_", dateString];
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *audio = [NSEntityDescription insertNewObjectForEntityForName:@"Audio" inManagedObjectContext:context];
    [audio setValue:[self.url absoluteString] forKey:@"filePath"];
    [audio setValue:time forKey:@"time"];
    if(!self.isNewRecording){
        if([self.audio.isNameChanged isEqualToString:@"YES"]){
            [audio setValue:@"YES" forKey:@"isNameChanged"];
            [audio setValue:self.audio.name forKey:@"name"];
        }
        else{
            [audio setValue:@"NO" forKey:@"isNameChanged"];
            [audio setValue:dateString forKey:@"name"];
        }
    }else{
        [audio setValue:@"NO" forKey:@"isNameChanged"];
        [audio setValue:dateString forKey:@"name"];
    }
    
    NSError *error = nil;
    if (![audio.managedObjectContext save:&error]) {
        NSLog(@"Save Failed! %@ %@", error, [error localizedDescription]);
    }else{
        userInfo = [UserInfo instance];
        userInfo.numberOfFreeRecordings = userInfo.numberOfFreeRecordings + 1;
        userInfo.audioSessionForVoice = NO;
        [userInfo saveUserInfo];
        [self.view hideToastActivity];
        //if(!self.isNewRecording){
        [self deleteAudio];
        //}
        NSString *message = @"";
        int remainingFreeRecordings = knumberOfFreeRecordingsCount - userInfo.numberOfFreeRecordings;
        if (!userInfo.isSubscribed) {
            if (remainingFreeRecordings == 0) {
                message = @"Youe trial recording are over now.You need to subscribe to use full features of \"Dragon Speech Recording\"";
            } else {
                message = [NSString stringWithFormat:@"Your audio is saved.Your can make %d trial audios more.Make sure to get subscribed to make unlimited audios when your trial is over",remainingFreeRecordings];
            }
            
        } else {
            message = @"Your Audio is saved";
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message: message delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(showRecordings) userInfo:nil repeats:NO];
    }

}
- (IBAction)saveAudio:(id)sender {
    [self addProperties];
}

- (void)sliderTapped:(UIGestureRecognizer *)g{
    if([player isPlaying]){
       // [player stop];
       // [self invalidateTimer];
    }
    tapCount = tapCount + 1;
    NSLog(@"Tap Count -- %d",tapCount);
    UISlider* s = (UISlider*)g.view;
    if (s.highlighted)
        return;
    CGPoint pt = [g locationInView: s];
    CGFloat percentage = pt.x / s.bounds.size.width;
    CGFloat delta = percentage * (s.maximumValue - s.minimumValue);
    CGFloat value = s.minimumValue + delta;
    [s setValue:value animated:YES];
    player.currentTime = (value/100)*player.duration;
    currTime = player.currentTime;
    //float progress = player.currentTime;
   // self.time.text = [NSString stringWithFormat:@"%.02f",progress/60];
    double hours  = player.currentTime / 3600 ;
    double minutes = fmod(trunc(player.currentTime / 60.0), 60.0);
    double seconds = fmodf(player.currentTime,60.0f);
    self.time.text = [NSString stringWithFormat: @"%02.f:%02.f:%02.f", hours, minutes, seconds];
    [self.view setNeedsDisplay];

}

- (IBAction)stopAudio:(id)sender {
    if([player isPlaying]){
        trimFlag = true;
        [player stop];
    }
}

- (IBAction)playAudio:(id)sender {
    if(![player isPlaying]){
        trimFlag = false;
        _flag = YES;
        [player setDelegate:self];
        [player play];
        
        if(!timer)
            timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSeekBar) userInfo:nil repeats:YES];
    }
}

- (IBAction)overloadAction:(id)sender {
    if([player isPlaying]){
        [player stop];
        [self invalidateTimer];
    }
    double endTime = player.currentTime;
    if(player.currentTime == 0.0)
        
        endTime = 0.1;
    [self trimAudio:endTime];
    AppDelegate *appdelegate = [self appDelegate];
    appdelegate.isInsert = NO;
    if(appdelegate.isFileSaved)
        appdelegate.isUpdated = YES;
    appdelegate.flag = YES;
}

- (IBAction)insertAudio:(id)sender {
    if([player isPlaying]){
        [player stop];
        [self invalidateTimer];
    }
   double endTime = player.currentTime;
    AppDelegate *appdelegate = [self appDelegate];
    appdelegate.isInsert = YES;
    appdelegate.flag = YES;
    if(appdelegate.isFileSaved)
        appdelegate.isUpdated = YES;
    if(player.currentTime == 0.0){
        endTime = player.duration;
       // appdelegate.filePath = nil;
        appdelegate.prevAudioPath = self.url;
        appdelegate.audioDuration = player.duration;
        [self recordOver];
        
    }
    else
        [self trimAudio:endTime];
}

#pragma mark - helper methods
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    context = [delegate managedObjectContext];
    return context;
}

-(AppDelegate*)appDelegate{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}

- (void)updateSeekBar{
    if(_flag){
        float progress = player.currentTime;
        float tt = player.duration;
        float p = (progress/tt)*100;
      //  self.time.text = [NSString stringWithFormat:@"%.02f",progress/60];
        [self.slider setValue:p];
        double hours  = player.currentTime / 3600 ;
        double minutes = fmodf(trunc(player.currentTime / 60.0), 60.0f);//player.duration / 60.0 %  60.0;
        double seconds = fmodf(player.currentTime,60.0f);
        self.time.text = [NSString stringWithFormat: @"%02.f:%02.f:%02.f", hours, minutes, seconds];
        [self.view setNeedsDisplay];
        [self.view setNeedsDisplay];
    }
}

- (void) mergeTwoAudioFiles{
    AppDelegate *delegate = [self appDelegate];
    //self.url is the new audio recorded after overload button clicked...
    //delegate.filePath is the audio which is the trimmed audio before the overload button is clicked.
    NSLog(@"self %@", [self.url absoluteString]);
    NSLog (@"delegate %@", [delegate.filePath absoluteString]);
    NSURL *audioURL1 = [NSURL fileURLWithPath:[delegate.filePath path]];
    NSURL *audioURL2 = [NSURL fileURLWithPath:[self.url path]];
    AVAsset *avAsset1 = [AVURLAsset URLAssetWithURL:audioURL1 options:nil];
    AVAsset *avAsset2 = [AVURLAsset URLAssetWithURL:audioURL2 options:nil];


    AVMutableComposition *composition = [[AVMutableComposition alloc] init];
    [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *track = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    NSLog(@"hello");
    AVAssetTrack *assetTrack1 = [[avAsset1 tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    AVAssetTrack *assetTrack2 = [[avAsset2 tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    CMTime insertionPoint = kCMTimeZero;
    [track insertTimeRange:CMTimeRangeMake(kCMTimeZero, avAsset1.duration) ofTrack:assetTrack1 atTime:insertionPoint error:nil];
    insertionPoint = CMTimeAdd(insertionPoint, avAsset1.duration);
    [track insertTimeRange:CMTimeRangeMake(kCMTimeZero, avAsset2.duration) ofTrack:assetTrack2 atTime:insertionPoint error:nil];
    NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"/" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@":" withString:@""];
    dateString = [NSString stringWithFormat:@"%@%@",@"UR_VOICE_", dateString];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:dateString];
    if(!_isMergedRemainingAudio )
        fullPath = [fullPath stringByAppendingFormat:@"_m"];
    else
        fullPath = [fullPath stringByAppendingFormat:@"_mg"];

    fullPath = [fullPath stringByAppendingFormat:@".m4a"];
    NSURL *audioFileOutput  = [NSURL fileURLWithPath:fullPath];
    AVAssetExportSession *exportSession = [AVAssetExportSession exportSessionWithAsset:composition presetName:AVAssetExportPresetAppleM4A];
    exportSession.outputURL = audioFileOutput;
    exportSession.outputFileType = AVFileTypeAppleM4A;
    
//    AVMutableMetadataItem *common1;
//    AVMutableMetadataItem *common2;
//    AVMutableMetadataItem *common3;
//    AVMutableMetadataItem *common4;
//    common1 = [[AVMutableMetadataItem alloc] init];
//    common1.identifier = AVMetadataCommonIdentifierTitle;
//
//    common1.keySpace = AVMetadataKeySpaceCommon;
//    common1.key = AVMetadataCommonKeyTitle;
//    common1.value = @"Title Test Value";
//    
//    common2 = [[AVMutableMetadataItem alloc] init];    // Description
//    common2.identifier =  AVMetadataCommonIdentifierAuthor;
//    
//    common2.keySpace = AVMetadataKeySpaceCommon;
//    common2.key = AVMetadataCommonKeyArtist;
//    common2.value = @"Abeera ";
//    
//    common3 = [[AVMutableMetadataItem alloc] init];    // Description
//    common3.identifier =  AVMetadataCommonIdentifierCopyrights;
//    
//    common3.keySpace = AVMetadataKeySpaceCommon;
//    common3.key = AVMetadataCommonKeyCopyrights;
//    common3.value = @"cr Test Value";
//    
//    NSArray *MyMetadata;
//
//    MyMetadata = [[NSArray alloc] initWithObjects:common1,common2,common3,nil];
//    exportSession.metadata = MyMetadata;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        if (AVAssetExportSessionStatusCompleted == exportSession.status) {
            NSLog(@"AVAssetExportSessionStatusCompleted");
        dispatch_async(dispatch_get_main_queue(), ^{
            if(_isMergedRemainingAudio )
                [self.view hideToastActivity];
        });
            [self enableViews];

        
                [self removeFile:delegate.filePath];
                [self removeFile:self.url];
                self.url = audioFileOutput;
                player = [[AVAudioPlayer alloc]
                          initWithContentsOfURL:audioFileOutput error:nil];
                NSLog(@"plyer duration, %f", player.duration);
                if(!_isMergedRemainingAudio )
                    [self overWriteAudio];
                else{
//                    delegate.filePath = nil;
//                    delegate.prevAudioPath = nil;
                    [self.view hideToastActivity];
               }
        } else if (AVAssetExportSessionStatusFailed == exportSession.status) {
            NSLog(@"AVAssetExportSessionStatusFailed");
            dispatch_async(dispatch_get_main_queue(), ^{
                    [self.view hideToastActivity];

            });
            [self enableViews];

//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Error occurred while processing" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//            [alert show];

        } else {
            NSLog(@"Export Session Status: %ld", (long)exportSession.status);
        }
    }];
}

-(void)overWriteAudio{
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    if(delegate.audioDuration < player.duration)
    NSLog(@"player duration %f", player.duration);
    NSLog(@"delegate duration %f", delegate.audioDuration);
    if(player.duration < delegate.audioDuration || delegate.isInsert){
//    BOOL flag = YES;
//    if(flag){
        [self MergeRemainingAudio];
    }else{
        delegate.audioDuration = 0.0;
        delegate.filePath = nil;
        [self.view hideToastActivity];

    }
}

-(BOOL) MergeRemainingAudio{
    _flag = NO;
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    float vocalStartMarker ;
    if(delegate.isInsert)
        vocalStartMarker = delegate.prevEndMarker;
    else
        vocalStartMarker = player.duration;

    float vocalEndMarker = delegate.audioDuration;
    NSURL *audioFileInput = delegate.prevAudioPath;
    NSLog(@"start %f", player.duration);
    NSLog(@"url %@", delegate.prevAudioPath);
    NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"/" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@":" withString:@""];
    dateString = [NSString stringWithFormat:@"%@%@",@"UR_VOICE_", dateString];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:dateString];
    fullPath = [fullPath stringByAppendingFormat:@"_th"];
    fullPath = [fullPath stringByAppendingFormat:@".m4a"];
    NSURL *audioFileOutput  = [NSURL fileURLWithPath:fullPath];
    if (!audioFileInput || !audioFileOutput){
        return NO;
    }
    [[NSFileManager defaultManager] removeItemAtURL:audioFileOutput error:NULL];
    AVAsset *asset = [AVAsset assetWithURL:audioFileInput];
    AVAssetExportSession *exportSession = [AVAssetExportSession exportSessionWithAsset:asset
                                                                            presetName:AVAssetExportPresetAppleM4A];
    
    
    if (exportSession == nil)
        return NO;
    CMTime startTime = CMTimeMake((int)(floor(vocalStartMarker * 100)),100);
    CMTime stopTime = CMTimeMake((int)(ceil(vocalEndMarker * 100)), 100);
    CMTimeRange exportTimeRange = CMTimeRangeFromTimeToTime(startTime, stopTime);
    exportSession.outputURL = audioFileOutput;
    exportSession.outputFileType = AVFileTypeAppleM4A;
    exportSession.timeRange = exportTimeRange;
    
//    
//    
//    AVMutableMetadataItem *common1;
//    AVMutableMetadataItem *common2;
//    AVMutableMetadataItem *common3;
//    AVMutableMetadataItem *common4;
//    common1 = [[AVMutableMetadataItem alloc] init];    // Title
//    common1.identifier = AVMetadataCommonIdentifierTitle;
//    common1.keySpace = AVMetadataKeySpaceCommon;
//    common1.key = AVMetadataCommonKeyTitle;
//    common1.value = @"Title Test Value";
//    
//    common2 = [[AVMutableMetadataItem alloc] init];    // Description
//    common2.identifier =  AVMetadataCommonIdentifierArtist;
//    
//    common2.keySpace = AVMetadataKeySpaceCommon;
//    common2.key = AVMetadataCommonKeyArtist;
//    common2.value = @"Abeera";
//    
//    common3 = [[AVMutableMetadataItem alloc] init];    // Description
//    common3.identifier =  AVMetadataCommonIdentifierCopyrights;
//    
//    common3.keySpace = AVMetadataKeySpaceCommon;
//    common3.key = AVMetadataCommonKeyCopyrights;
//    common3.value = @"cr Test Value";
//
//    NSArray *MyMetadata;
//
//    MyMetadata = [[NSArray alloc] initWithObjects:common1,common2,common3,nil];
//    exportSession.metadata = MyMetadata;

    [exportSession exportAsynchronouslyWithCompletionHandler:^
     {
         NSLog(@"status , %ld", (long)exportSession.status);
         if (AVAssetExportSessionStatusCompleted == exportSession.status){
             // It worked!
             NSLog(@"It worked");
             // self.url = audioFileOutput;
             //             player = [[AVAudioPlayer alloc]
             //                       initWithContentsOfURL:self.url error:nil];
             
             delegate.filePath = self.url;
             self.url = audioFileOutput;
//             self.Remainingurl = audioFileOutput;
             delegate.flag = false;
            dispatch_async(dispatch_get_main_queue(), ^{
                 _isMergedRemainingAudio = YES;
                 [self mergeTwoAudioFiles];
                 [self performSelector:@selector(remove:) withObject:nil afterDelay:0.2];
             });
         } else if (AVAssetExportSessionStatusFailed == exportSession.status){
             NSLog(@"It Failed");
         }
     }];
    return YES;
}

-(IBAction)remove:(id)sender{
//    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    [self removeFile:delegate.prevAudioPath];
    AppDelegate *delegate = [self appDelegate];
//    if(url == delegate.filePath)
    if(!delegate.isFileSaved || ![delegate.audio.filePath isEqualToString:[delegate.prevAudioPath absoluteString]]) {
        NSURL* url = delegate.prevAudioPath;
        delegate.prevAudioPath = nil;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        // BOOL success = [fileManager removeItemAtPath:[delegate.filePath path] error:&error];
        //agr ye delete kar dia ha, to self.url me to kuch bhi nhi hona chahye., mtlb khaali url ho or us url per audio kuch na pari ho.
        BOOL success = [fileManager removeItemAtPath:[url path] error:&error];
        if (success) {
            NSLog(@"File deleted");
            NSLog(@"self url, %@", self.url);
            //        UIAlertView *removedSuccessFullyAlert = [[UIAlertView alloc] initWithTitle:@"Congratulations:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            //        [removedSuccessFullyAlert show];
        } else{
            NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
        }
    }

}
- (BOOL)trimAudio:(double) endTime{
    _flag = YES;
    float vocalStartMarker = 0.0;
    float vocalEndMarker = endTime;
    NSURL *audioFileInput =self.url;
    NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"/" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@":" withString:@""];
    dateString = [NSString stringWithFormat:@"%@%@",@"UR_VOICE_", dateString];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:dateString];
    fullPath = [fullPath stringByAppendingFormat:@"_t"];
    fullPath = [fullPath stringByAppendingFormat:@".m4a"];
    NSURL *audioFileOutput  = [NSURL fileURLWithPath:fullPath];
    if (!audioFileInput || !audioFileOutput){
        return NO;
    }
    [[NSFileManager defaultManager] removeItemAtURL:audioFileOutput error:NULL];
    AVAsset *asset = [AVAsset assetWithURL:audioFileInput];
    AVAssetExportSession *exportSession = [AVAssetExportSession exportSessionWithAsset:asset
                                                                            presetName:AVAssetExportPresetAppleM4A];
    if (exportSession == nil)
        return NO;
    CMTime startTime = CMTimeMake((int)(floor(vocalStartMarker * 100)),100);
    CMTime stopTime = CMTimeMake((int)(ceil(vocalEndMarker * 100)), 100);
    CMTimeRange exportTimeRange = CMTimeRangeFromTimeToTime(startTime, stopTime);
    exportSession.outputURL = audioFileOutput;
    exportSession.outputFileType = AVFileTypeAppleM4A;
    exportSession.timeRange = exportTimeRange;
    
    
//    
//    AVMutableMetadataItem *common1;
//    AVMutableMetadataItem *common2;
//    AVMutableMetadataItem *common3;
//    AVMutableMetadataItem *common4;
//    common1 = [[AVMutableMetadataItem alloc] init];    // Title
//    common1.keySpace = AVMetadataKeySpaceCommon;
//    common1.identifier =  AVMetadataCommonIdentifierTitle;
//    common1.key = AVMetadataCommonKeyTitle;
//    common1.value = @"Title Test Value";
//    
//    
//    common2 = [[AVMutableMetadataItem alloc] init];    // Description
//    common2.identifier =  AVMetadataCommonIdentifierArtist;
//    
//    common2.keySpace = AVMetadataKeySpaceCommon;
//    common2.key = AVMetadataCommonKeyArtist;
//    common2.value = @"Abeera";
//    
//    common3 = [[AVMutableMetadataItem alloc] init];    // Description
//    common3.identifier =  AVMetadataCommonIdentifierCopyrights;
//    
//    common3.keySpace = AVMetadataKeySpaceCommon;
//    common3.key = AVMetadataCommonKeyCopyrights;
//    common3.value = @"cr Test Value";
//    
//    NSArray *MyMetadata;
//    
//    MyMetadata = [[NSArray alloc] initWithObjects:common1,common2,common3,nil];
//    exportSession.metadata = MyMetadata;
    
    
    [exportSession exportAsynchronouslyWithCompletionHandler:^
     {
         if (AVAssetExportSessionStatusCompleted == exportSession.status){
             NSLog(@"It worked");
             // self.url = audioFileOutput;
             //             player = [[AVAudioPlayer alloc]
             //                       initWithContentsOfURL:self.url error:nil];
             AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
             delegate.filePath = audioFileOutput;
             delegate.flag = true;
             NSLog(@"trimmed %@",[audioFileOutput absoluteString]);
             dispatch_async(dispatch_get_main_queue(), ^{
                 // [self removeFile:self.url];
                 //duration is the full duration...
                 delegate.audioDuration = player.duration;
                 //saving full url...
                 delegate.prevAudioPath = self.url;
                 delegate.prevEndMarker = vocalEndMarker;
                 
                 [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(recordOver) userInfo:nil repeats:NO];
             });
         } else if (AVAssetExportSessionStatusFailed == exportSession.status){
             NSLog(@"It Failed");
         }
     }];
    return YES;
}

- (void)removeFile:(NSURL*)url{
    AppDelegate *delegate = [self appDelegate];
    if(url == delegate.filePath)
        delegate.filePath = nil;
    if(!delegate.isFileSaved || ![delegate.audio.filePath isEqualToString:[url absoluteString]]) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        BOOL success = [fileManager removeItemAtPath:[url path] error:&error];
        if (success) {
            NSLog(@"File deleted");
            NSLog(@"self url, %@", self.url);
           } else{
            NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
        }
    }
}

- (void) invalidateTimer{
    if([timer isValid]){
        [timer invalidate];
        timer = nil;
    }
}

- (void) showRecordings{
    UIViewController *View = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3];
    [self.navigationController popToViewController:View animated:YES];
}

- (void) recordOver{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)handleBack:(id)sender{
    dispatch_async(dispatch_get_main_queue(), ^{
    AppDelegate *delegate = [self appDelegate];
        if(delegate.filePath)
            [self removeFile:delegate.filePath];
        if(delegate.prevAudioPath)
            [self removeFile:delegate.prevAudioPath];
        delegate.filePath = nil;
        delegate.prevAudioPath = nil;
        delegate.flag = NO;
        if(self.isNewRecording){
            RecordingViewController *rvc;
            UIImage *closebtnimg = [UIImage imageNamed:@"record"];
            [rvc.recordBtn setImage: closebtnimg forState:UIControlStateNormal];
            [self.navigationController popViewControllerAnimated:YES];
        }else{        
            NSArray *array = [self.navigationController viewControllers];
            UIImage *closebtnimg = [UIImage imageNamed:@"record"];
            RecordingViewController *rvc;
            [rvc.recordBtn setImage: closebtnimg forState:UIControlStateNormal];
            [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];
        }
    });
}

-(void) enableViews {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.processing.hidden = YES;
        self.overload.userInteractionEnabled = YES;
        self.insert.userInteractionEnabled = YES;
        self.slider.userInteractionEnabled = YES;
        self.play.userInteractionEnabled = YES;
        self.stop.userInteractionEnabled = YES;
        [self.view hideToastActivity];
    });
 //   [self.view hideToastActivity];
}

- (void)changeName:(id)sender{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Update File Name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    UITextField * alertTextField = [alert textFieldAtIndex:0];
    alertTextField.placeholder = @"";
    [alert show];
    
}
#pragma mark-alertview delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1){
        NSString *fileName   = [alertView textFieldAtIndex:0].text;
        if([fileName length] > 0){
            AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Audio" inManagedObjectContext:context];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"filePath == %@", self.audio.filePath];
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            [request setEntity:entityDesc];
            [request setPredicate:predicate];
            NSError *error;
            NSMutableArray *audios = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:request error:&error]];
            Audio* audio = [audios objectAtIndex:0];
            audio.name = fileName;
            audio.isNameChanged = @"YES";
            if (![context save:&error])
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
}



- (BOOL)addProperties{
    [self.view makeToastActivity];
    float vocalStartMarker = 0.0;
    NSURL *audioFileInput = self.url;
    NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"/" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@":" withString:@""];
    dateString = [NSString stringWithFormat:@"%@%@",@"UR_VOICE_", dateString];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:dateString];
    fullPath = [fullPath stringByAppendingFormat:@"_t"];
    fullPath = [fullPath stringByAppendingFormat:@".m4a"];
    NSURL *audioFileOutput  = [NSURL fileURLWithPath:fullPath];
    if (!audioFileInput || !audioFileOutput){
        return NO;
    }
    [[NSFileManager defaultManager] removeItemAtURL:audioFileOutput error:NULL];
    AVAsset *asset = [AVAsset assetWithURL:audioFileInput];
    AVAssetExportSession *exportSession = [AVAssetExportSession exportSessionWithAsset:asset
                                                                            presetName:AVAssetExportPresetAppleM4A];
    //float vocalEndMarker = asset.duration;
    
    if (exportSession == nil)
        return NO;
    CMTime startTime = CMTimeMake((int)(floor(vocalStartMarker * 100)),100);
    CMTime stopTime = asset.duration;
    CMTimeRange exportTimeRange = CMTimeRangeFromTimeToTime(startTime, stopTime);
    exportSession.outputURL = audioFileOutput;
    exportSession.outputFileType = AVFileTypeAppleM4A;
    exportSession.timeRange = exportTimeRange;
    
    
    
    AVMutableMetadataItem *common1;
    AVMutableMetadataItem *common2;
    AVMutableMetadataItem *common3;
    AVMutableMetadataItem *common4;
    common1 = [[AVMutableMetadataItem alloc] init];    // Title
    common1.keySpace = AVMetadataKeySpaceCommon;
    common1.identifier =  AVMetadataCommonIdentifierTitle;
    common1.key = AVMetadataCommonKeyTitle;
    common1.value = @"Title Test Value";
    
    
    common2 = [[AVMutableMetadataItem alloc] init];    // Description
    common2.identifier =  AVMetadataCommonIdentifierArtist;
    
    common2.keySpace = AVMetadataKeySpaceCommon;
    common2.key = AVMetadataCommonKeyArtist;
    common2.value = @"Abeera";
    
    common3 = [[AVMutableMetadataItem alloc] init];    // Description
    common3.identifier =  AVMetadataCommonIdentifierCopyrights;
    
    common3.keySpace = AVMetadataKeySpaceCommon;
    common3.key = AVMetadataCommonKeyCopyrights;
    common3.value = @"cr Test Value";
    
    NSArray *MyMetadata;
    
    MyMetadata = [[NSArray alloc] initWithObjects:common1,common2,common3,nil];
    exportSession.metadata = MyMetadata;
    
    
    [exportSession exportAsynchronouslyWithCompletionHandler:^
     {
         if (AVAssetExportSessionStatusCompleted == exportSession.status){
             NSLog(@"It worked");
             NSFileManager *fileManager = [NSFileManager defaultManager];
             NSError *error;
             // BOOL success = [fileManager removeItemAtPath:[delegate.filePath path] error:&error];
             //agr ye delete kar dia ha, to self.url me to kuch bhi nhi hona chahye., mtlb khaali url ho or us url per audio kuch na pari ho.
             BOOL success = [fileManager removeItemAtPath:[audioFileInput path] error:&error];
             if (success) {
                 NSLog(@"File deleted");
             } else{
                 NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
             }

             self.url = audioFileOutput;
             [self performSelectorOnMainThread:@selector(saveAudioInDb) withObject:nil waitUntilDone:NO];

            
         } else if (AVAssetExportSessionStatusFailed == exportSession.status){
             NSLog(@"It Failed");
         }
     }];
    return YES;
}

- (void) loadAlertView {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:@"By doing this your current recording will become empty"
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleCancel
                                handler:^(UIAlertAction * action) {
                                        [self showRecordings];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
}

@end

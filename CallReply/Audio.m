//
//  Audio.m
//  CallReply
//
//  Created by apple on 3/14/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "Audio.h"

@implementation Audio
@dynamic filePath;
@dynamic time;
@dynamic name;
#pragma mark Hooks
- (void)willInsertObject
{
    // If you extend XMPPMessageArchiving_Contact_CoreDataObject,
    // you can override this method to use as a hook to set your own custom properties.
}

- (void)didUpdateObject
{
    // If you extend XMPPMessageArchiving_Contact_CoreDataObject,
    // you can override this method to use as a hook to update your own custom properties.
}

@end

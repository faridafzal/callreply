//
//  AppDelegate.m
//  CallReply
//
//  Created by apple on 3/10/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "RageIAPHelper.h"
#import "Constants.h"
#import "UserInfo.h"
#import "Toast+UIView.h"
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate () {
    NSManagedObjectContext *moc;
    UITabBarController *tabBarController;
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    NSUbiquitousKeyValueStore* store = [NSUbiquitousKeyValueStore
                                        defaultStore];
    if (store) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(storeChanged:)
                                                     name:NSUbiquitousKeyValueStoreDidChangeExternallyNotification
                                                   object:store];
        [store synchronize];
    }
    [RageIAPHelper sharedInstance];
    
    [self addSkipBackupAttributeToItemAtURL:[self applicationDocumentDirectory]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *iCloudURL = [fileManager URLForUbiquityContainerIdentifier:nil];
    NSLog(@"%@", [iCloudURL absoluteString]);
    
    if(iCloudURL){
        NSUbiquitousKeyValueStore *iCloudStore = [NSUbiquitousKeyValueStore defaultStore];
        [iCloudStore setString:kSuccess forKey:kiCloudStatus];
        [iCloudStore synchronize]; // For Synchronizing with iCloud Server
        NSLog(@"iCloud status : %@", [iCloudStore stringForKey:kiCloudStatus]);
    }
    [self loadProducts];
    [self setHomeMenu];
    
    CGRect Frame = [[UIScreen mainScreen] bounds];
    //Assign the first window or launch first screen controller
    self.window = [[UIWindow alloc] initWithFrame:Frame];
    HomeViewController  *homeVC = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:homeVC];
    // [self.window addSubview:navController.view];
    [self.window makeKeyAndVisible];
    self.filePath = nil;
    self.flag = false;
    self.prevAudioPath = nil;
    self.isNewRecording = YES;
    self.audioDuration = 0.0;
    [self loadTabBar];
//    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
//    self.navigationController.navigationBar.barTintColor =  [UIColor colorWithRed:(195/255.0) green:(195/255.0) blue:(195/255.0) alpha:1];
//    self.navigationController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//
//    self.navigationController.navigationBar.translucent = NO;
//    [self.window setRootViewController:self.navigationController];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

#pragma mark - Helper Methods

- (void)loadTabBar {
    [[UITabBar appearance] setBarTintColor: [UIColor colorWithRed:(78.0/255.0) green:(154.0/255.0) blue:(212/255.0) alpha:1]];
    tabBarController = [[UITabBarController alloc] init];
    ShowRecordingViewController *srvc = [[ShowRecordingViewController alloc] initWithNibName:@"ShowRecordingViewController" bundle:nil];
    UINavigationController *voiceNavController = [[UINavigationController alloc] initWithRootViewController:srvc];
//    SpeechTextViewController *stvc = [[SpeechTextViewController alloc] initWithNibName:@"SpeechTextViewController" bundle:nil];
//    UINavigationController *speechNavController = [[UINavigationController alloc] initWithRootViewController:stvc];
        HomeViewController *stvc = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        UINavigationController *speechNavController = [[UINavigationController alloc] initWithRootViewController:stvc];
    srvc.tabBarItem.title = @"Voice Recording";
    srvc.tabBarItem.image = [UIImage imageNamed:@"speechRecorder"];
    stvc.tabBarItem.title = @"Speech To Text";
    stvc.tabBarItem.image = [UIImage imageNamed:@"textRecorder"];
    tabBarController.viewControllers = [NSArray arrayWithObjects:
                                        voiceNavController,
                                        speechNavController,
                                        nil];
    voiceNavController.navigationBar.tintColor = [UIColor whiteColor];
//    voiceNavController.navigationBar.barTintColor =  [UIColor colorWithRed:(195/255.0) green:(195/255.0) blue:(195/255.0) alpha:1];
    voiceNavController.navigationBar.barTintColor =  [UIColor colorWithRed:(78.0/255.0) green:(154.0/255.0) blue:(212.0/255.0) alpha:1];
    voiceNavController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [voiceNavController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    speechNavController.navigationBar.tintColor = [UIColor whiteColor];
//    speechNavController.navigationBar.barTintColor =  [UIColor colorWithRed:(195/255.0) green:(195/255.0) blue:(195/255.0) alpha:1];
    speechNavController.navigationBar.barTintColor =  [UIColor colorWithRed:(78.0/255.0) green:(154.0/255.0) blue:(212/255.0) alpha:1];
    speechNavController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    speechNavController.navigationBar.translucent = NO;
    voiceNavController.navigationBar.translucent = NO;
    [speechNavController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
   // tabBarController.tabBar.tintColor = [UIColor blackColor];
    tabBarController.tabBar.tintColor = [UIColor whiteColor];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
        tabBarController.tabBar.unselectedItemTintColor = [UIColor blackColor];
    }
    tabBarController.delegate = self;
    self.window.rootViewController = tabBarController;
}

- (BOOL)tabBarController:(UITabBarController *)tbController shouldSelectViewController:(UIViewController *)viewController {
    UserInfo *userInfo = [UserInfo instance];
    if (viewController == [tbController.viewControllers objectAtIndex:1] ){
        if (userInfo.audioSessionForVoice){
            [[NSNotificationCenter defaultCenter] postNotificationName:
             @"ExpireSession" object:nil userInfo:nil];
            return NO;
        }
    } else if (viewController == [tbController.viewControllers objectAtIndex:0] ){
        if (userInfo.audioSessionForText){
            [[NSNotificationCenter defaultCenter] postNotificationName:
             @"ExpireSession" object:nil userInfo:nil];
            return NO;
        }
    }
    return YES;
}

#pragma mark - Core Data stack

-(NSManagedObjectContext *)managedObjectContext {
    
    if (_managedObjectContext) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return _managedObjectModel;
}


/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"app.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}
#pragma mark -
#pragma mark Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(void)loadProducts
{
    [[RageIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            _products = products;
        }
    }];
}


- (NSURL *)applicationDocumentDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}


-(void)setHomeMenu
{
    NSUbiquitousKeyValueStore* store = [NSUbiquitousKeyValueStore
                                    defaultStore];
    if(store)
    {
        NSMutableDictionary *appstate = [store objectForKey:kappState];
        if(appstate)
        {
            NSDate *expireDate = [appstate valueForKey:kExpireDate];
            NSDate *c = [NSDate date];
            if ([expireDate compare:c] == NSOrderedDescending) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kuserIsSubscribed];
            } else if ([expireDate compare:[NSDate date]] == NSOrderedAscending) {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kuserIsSubscribed];
            } else {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kuserIsSubscribed];
            }
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kuserIsSubscribed];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}

- (void)getSubscribed {
    if (_products.count > 0){
        //dispatch_async(dispatch_get_main_queue(), ^{
            [tabBarController.view makeToastActivity];
      //  });
        SKProduct *product = self.products[0];
        if (product) {
            [[RageIAPHelper sharedInstance] buyProduct:product];
        }
    }
}

- (void)restoreSubscription {
    if (_products.count > 0){
       // dispatch_async(dispatch_get_main_queue(), ^{
            [tabBarController.view makeToastActivity];
        //});
        SKProduct *product = self.products[0];
        if (product) {
            [[RageIAPHelper sharedInstance] restoreCompletedTransactions];
        }
    }
}


- (void)hideToastActivity {
    dispatch_async(dispatch_get_main_queue(), ^{
        [tabBarController.view hideToastActivity];
    });
}

@end

//
//  HomeViewController.m
//  CallReply
//
//  Created by apple on 3/10/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "HomeViewController.h"
#import "ShowRecordingViewController.h"
#import "RecordinglTableViewCell.h"
#import "AppDelegate.h"
#import "UserInfo.h"
#import "Text.h"
#import "Constants.h"
#import "SubscriptionViewController.h"
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone)

@interface HomeViewController (){
    NSMutableArray *texts;
    UserInfo *userInfo;
    Text *textToRename;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Speech To Text"; //@"Speech Recording";
//    if ([self.titleString isEqualToString:@"Speech Recording"]) {
//        self.voiceRecordingBtn.hidden = NO;
//        self.speechToTextBtn.hidden = YES;
//    } else {
//        self.speechToTextBtn.hidden = NO;
//        self.voiceRecordingBtn.hidden = YES;
//
//    }
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.cellArray = [[NSMutableArray alloc] init];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
   // [self fetchTexts];

}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    userInfo = [UserInfo instance];
    userInfo.audioSessionForVoice = NO;
    if(userInfo.audioSessionForText){
        userInfo.audioSessionForText = NO;
        [userInfo saveUserInfo];
        [self.tabBarController setSelectedIndex: 0];
    }
    [self fetchTexts];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).filePath = nil;
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).flag = false;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)fetchTexts{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Text" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSError *error;
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"time" ascending:NO];
    
    texts = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:request error:&error]];
    texts = [[NSMutableArray alloc]initWithArray:[texts sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]]];
    [self.tableView reloadData];
}

- (IBAction)sppechToTextBtn:(id)sender {
    if (userInfo.isSubscribed || userInfo.numberOfFreeTexts < kNumberOfFreeTextsCount) {
        userInfo.audioSessionForText = NO;
        [userInfo saveUserInfo];
        SpeechTextViewController *stvc = [[SpeechTextViewController alloc] initWithNibName:@"SpeechTextViewController" bundle:nil];
        stvc.isNewRecording = YES;
        [self.navigationController pushViewController:stvc animated:YES];
    } else {
        [self askUserForSubscription];
    }
}

#pragma mark - Swipeable TableViewCell Delegate functions

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    NSInteger tag = cell.tag-10000;
    Text *text = (Text*)[texts objectAtIndex:tag];
    if (index == 1) {
        [self deleteText:text];
    } else {
        textToRename = text;
        [self changeName];
    }
}


- (void)deleteText:(Text*)text{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *audioEntity=[NSEntityDescription entityForName:@"Text" inManagedObjectContext:context];
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:audioEntity];
    NSPredicate *p=[NSPredicate predicateWithFormat:@"time == %@", text.time];
    [fetch setPredicate:p];
    NSError *fetchError;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *text in fetchedProducts) {
        [context deleteObject:text];
        NSError *error = nil;
        if (![context save:&error]){
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self fetchTexts];
    });
}



- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
       [UIColor colorWithRed:78.0/255.0f green:154.0/255.0 blue:212.0/255.0 alpha:1.0]
                                                title:@"Rename"];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.9960f green:0.247f blue:0.207 alpha:1.0f]
                                                title:@"Trash"];
    
    
    return rightUtilityButtons;
}


#pragma mark - TableViewCell Delegate Functions

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [texts count];    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Text *text = (Text*)[texts objectAtIndex:indexPath.row];
    static NSString *MyIdentifier = @"recordingCell";
    RecordinglTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RecordinglTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.rightUtilityButtons = [self rightButtons];
      //  [cell setSeparatorInset:UIEdgeInsetsZero];
        [cell setDelegate:self];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = 10000+indexPath.row;
    [cell.shareAudio addTarget:self action:@selector(shareText:) forControlEvents:UIControlEventTouchUpInside];
    NSArray* fileNameArr = [text.name componentsSeparatedByString: @"_"];
    cell.shareAudio.tag = 10000+indexPath.row;
    NSInteger count = indexPath.row + 1;
    NSString* str ;
    if([text.isNameChanged isEqualToString:@"NO"])
        str = [NSString stringWithFormat:@"%@_%@_%@_%ld",[fileNameArr objectAtIndex:0] ,[fileNameArr objectAtIndex:1],[fileNameArr objectAtIndex:2], (long)count];
    else
        str = text.name;
    cell.audiofileName.text = str;
    cell.contentView.tag = 10000+indexPath.row;
    UITapGestureRecognizer *cellTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openText:)];
    [cell.contentView addGestureRecognizer:cellTapRecognizer];
    if(![self.cellArray containsObject:cell])
        [self.cellArray insertObject:str atIndex:indexPath.row];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"cell selected");
}

-(void)openText:(UITapGestureRecognizer*)sender{
    if(userInfo.isSubscribed || userInfo.numberOfFreeTexts < kNumberOfFreeTextsCount) {
        userInfo.audioSessionForText = YES;
        [userInfo saveUserInfo];
        SpeechTextViewController *stvc = [[SpeechTextViewController alloc] initWithNibName:@"SpeechTextViewController" bundle:nil];
        Text *text = (Text*)[texts objectAtIndex:sender.view.tag-10000];
        stvc.isNewRecording = NO;
        stvc.formerText = text;
        [self.navigationController pushViewController:stvc animated:YES];
    } else {
        [self askUserForSubscription];
    }
}

- (void)changeName {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Update File Name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
    alert.tag = 10001;
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    UITextField * alertTextField = [alert textFieldAtIndex:0];
    alertTextField.placeholder = @"";
    [alert show];
    
}
-(void)shareText:(UIButton*)sender{
    NSInteger tag = sender.tag - 10000;
    Text *text = (Text*)[texts objectAtIndex:tag];
    NSString *textToShare = text.text;
        NSArray *activityItems;
        activityItems = @[textToShare];
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        [activityViewController setValue:@"Speech Text" forKey:@"subject"];
        if (IS_IPAD) {
            activityViewController.popoverPresentationController.sourceView = self.view;
        }
        [activityViewController setCompletionHandler:^(NSString *activityType, BOOL completed) {
            if (!completed) {
                return;
            }
            else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                    message: @"Text is shared successfully"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
            }
        }];
        [self presentViewController:activityViewController animated:YES completion:nil];
}

- (void)askUserForSubscription {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Subscription"
                                 message:@"Please subscribe to use full features of \"Dragon Speech Recording\""
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* subscribe = [UIAlertAction
                                actionWithTitle:@"Subscribe"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    SubscriptionViewController *subscriptionViewController = [[SubscriptionViewController alloc] init];
                                    [self.navigationController pushViewController:subscriptionViewController animated:YES];
//                                    [((AppDelegate*)[[UIApplication sharedApplication] delegate]) getSubscribed];
                                }];
    UIAlertAction* retore = [UIAlertAction
                             actionWithTitle:@"Restore"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 [((AppDelegate*)[[UIApplication sharedApplication] delegate]) restoreSubscription];
                             }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:nil];
    [alert addAction:subscribe];
    [alert addAction:retore];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 10001){
        if(buttonIndex == 1){
            NSString *fileName   = [alertView textFieldAtIndex:0].text;
            if([fileName length] > 0){
                AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                NSManagedObjectContext *context = [appDelegate managedObjectContext];
                NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Text" inManagedObjectContext:context];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"time == %@", textToRename.time];
                NSFetchRequest *request = [[NSFetchRequest alloc] init];
                [request setEntity:entityDesc];
                [request setPredicate:predicate];
                NSError *error;
                NSMutableArray *audioArr = [[NSMutableArray alloc]initWithArray:[context executeFetchRequest:request error:&error]];
                Text* audio = [audioArr objectAtIndex:0];
                audio.name = fileName;
                audio.isNameChanged = @"YES";
                if (![context save:&error])
                    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self fetchTexts];
                });
            }
        }

    }
}
@end

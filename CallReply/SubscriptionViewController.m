//
//  SubscriptionViewController.m
//  TranscribeForMe
//
//  Created by Badar Iqbal on 16/04/2018.
//  Copyright © 2018 tbox. All rights reserved.
//

#import "SubscriptionViewController.h"
#import "AppDelegate.h"

@interface SubscriptionViewController ()

@end

@implementation SubscriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self.view layoutIfNeeded];
    CGSize sizeThatFitsTextView = [self.textView sizeThatFits:(CGSizeMake(self.textView.frame.size.width, (MAXFLOAT)))];
    CGRect textViewFrame = self.textView.frame;
    textViewFrame.size.height = sizeThatFitsTextView.height + 15;
    
    CGRect buttonFrame = self.privacyPolicyButton.frame;
    buttonFrame.origin.y = self.textView.frame.origin.y + sizeThatFitsTextView.height;
    self.privacyPolicyButton.frame = buttonFrame;
    buttonFrame.origin.y = buttonFrame.origin.y + 38;
    self.termsOfServiceButton.frame = buttonFrame;
    
    CGRect scrollContainer = self.scrollViewContainer.frame;
    scrollContainer.size =  CGSizeMake(self.scrollViewContainer.frame.size.width, 135 +  self.textView.frame.origin.y + sizeThatFitsTextView.height);
    self.scrollViewContainer.frame = scrollContainer;
    self.scrollView.contentSize = scrollContainer.size;
    //80 + self.textView.frame.origin.y + sizeThatFitsTextView.height;
}
    


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.tabBarController.tabBar.hidden = true;
    
    [self.view layoutIfNeeded];
    CGSize sizeThatFitsTextView = [self.textView sizeThatFits:(CGSizeMake(self.textView.frame.size.width, (MAXFLOAT)))];
    CGRect textViewFrame = self.textView.frame;
    textViewFrame.size.height = sizeThatFitsTextView.height + 15;
    
    CGRect buttonFrame = self.privacyPolicyButton.frame;
    buttonFrame.origin.y = self.textView.frame.origin.y + sizeThatFitsTextView.height;
    self.privacyPolicyButton.frame = buttonFrame;
    buttonFrame.origin.y = buttonFrame.origin.y + 38;
    self.termsOfServiceButton.frame = buttonFrame;
    
    CGRect scrollContainer = self.scrollViewContainer.frame;
    scrollContainer.size =  CGSizeMake(self.scrollViewContainer.frame.size.width, 135 +  self.textView.frame.origin.y + sizeThatFitsTextView.height);
    self.scrollViewContainer.frame = scrollContainer;
    self.scrollView.contentSize = scrollContainer.size;
    //80 + self.textView.frame.origin.y + sizeThatFitsTextView.height;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.tabBarController.tabBar.hidden = false;
}

- (IBAction)openPrivacyPolicy:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.transcribeforme.ca/privacy-policy/"]];
}

- (IBAction)openTermsOfService:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.transcribeforme.ca/terms-conditions/"]];
}

- (IBAction)subscribe:(id)sender {
    //self.subscribeButton.enabled = false;
//    [self dismissViewControllerAnimated:YES completion:^{
//         [((AppDelegate*)[[UIApplication sharedApplication] delegate]) getSubscribed];
//    }];
    [self.navigationController popViewControllerAnimated:false];
    [((AppDelegate*)[[UIApplication sharedApplication] delegate]) getSubscribed];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end

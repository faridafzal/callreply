//
//  Utility.m
//  CallReply
//
//  Created by apple on 10/26/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "Utility.h"

@implementation Utility
+ (BOOL) convertAudio:(NSString *)filePath {
    NSURL *assetURL = [NSURL URLWithString:filePath];
    AVURLAsset *songAsset = [AVURLAsset URLAssetWithURL:assetURL options:nil];
    NSError *assetError = nil;
    AVAssetReader *assetReader = [AVAssetReader assetReaderWithAsset:songAsset
                                                               error:&assetError];
    if (assetError) {
        NSLog (@"error: %@", assetError);
        return NO;
    }
    AVAssetReaderOutput *assetReaderOutput = [AVAssetReaderAudioMixOutput
                                              assetReaderAudioMixOutputWithAudioTracks:songAsset.tracks
                                              audioSettings: nil];
    if (! [assetReader canAddOutput: assetReaderOutput]) {
        NSLog (@"can't add reader output... die!");
        return NO;
    }
    [assetReader addOutput: assetReaderOutput];
    NSString *strcafFileName = [NSString stringWithFormat:@"%@_c.wav",[filePath stringByDeletingPathExtension]];
    NSURL *exportURL = [NSURL URLWithString:strcafFileName];
    AVAssetWriter *assetWriter = [AVAssetWriter assetWriterWithURL:exportURL
                                                          fileType:AVFileTypeWAVE
                                                             error:&assetError];
    if (assetError) {
        NSLog (@"error: %@", assetError);
        return NO;
    }
    AudioChannelLayout channelLayout;
    memset(&channelLayout, 0, sizeof(AudioChannelLayout));
    channelLayout.mChannelLayoutTag = kAudioChannelLayoutTag_Stereo;
    NSDictionary *outputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt:kAudioFormatLinearPCM], AVFormatIDKey,
                                    [NSNumber numberWithFloat:11025], AVSampleRateKey,
                                    [NSNumber numberWithInt:2], AVNumberOfChannelsKey,
                                    [NSData dataWithBytes:&channelLayout length:sizeof(AudioChannelLayout)], AVChannelLayoutKey,
                                    [NSNumber numberWithInt:16], AVLinearPCMBitDepthKey,
                                    [NSNumber numberWithBool:NO], AVLinearPCMIsNonInterleaved,
                                    [NSNumber numberWithBool:NO],AVLinearPCMIsFloatKey,
                                    [NSNumber numberWithBool:NO], AVLinearPCMIsBigEndianKey,
                                    nil];
    AVAssetWriterInput *assetWriterInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio
                                                                              outputSettings:outputSettings];
    if ([assetWriter canAddInput:assetWriterInput]) {
        [assetWriter addInput:assetWriterInput];
    } else {
        NSLog(@"can't add asset writer input... die!");
        return NO;
    }
    assetWriterInput.expectsMediaDataInRealTime = NO;
    [assetWriter startWriting];
    [assetReader startReading];
    AVAssetTrack *soundTrack = [songAsset.tracks objectAtIndex:0];
    CMTime startTime = CMTimeMake (0, soundTrack.naturalTimeScale);
    [assetWriter startSessionAtSourceTime: startTime];
    __block UInt64 convertedByteCount = 0;
    dispatch_group_t taskGroup = dispatch_group_create();
    dispatch_queue_t mediaInputQueue = dispatch_queue_create("mediaInputQueue", NULL);
    dispatch_group_enter(taskGroup);
    [assetWriterInput requestMediaDataWhenReadyOnQueue:mediaInputQueue
                                            usingBlock: ^ {
         while (assetWriterInput.readyForMoreMediaData) {
             CMSampleBufferRef nextBuffer = [assetReaderOutput copyNextSampleBuffer];
             if (nextBuffer) {
                 // append buffer
                 [assetWriterInput appendSampleBuffer: nextBuffer];
                 convertedByteCount += CMSampleBufferGetTotalSampleSize (nextBuffer);
                 CMSampleBufferInvalidate(nextBuffer);
                 CFRelease(nextBuffer);
                 nextBuffer = NULL;
             } else {
                 [assetWriterInput markAsFinished];
                 [assetReader cancelReading];
                 break;
             }
         }
         dispatch_group_leave(taskGroup);
     }];
    dispatch_group_wait(taskGroup, DISPATCH_TIME_FOREVER);
    return YES;
}

+ (BOOL) createTextFile: (NSString*)filePath
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *CreationDate = [formatter stringFromDate:[NSDate date]];
    
    NSURL *audioFileURL = [NSURL URLWithString:filePath];
    
    AVURLAsset* audioAsset = [AVURLAsset URLAssetWithURL:audioFileURL options:nil];
    CMTime audioDuration = audioAsset.duration;
    float audioDurationSeconds = CMTimeGetSeconds(audioDuration);
    audioDurationSeconds = ceilf(audioDurationSeconds * 1000);
    NSLog(@"Audio File Duration: %f", audioDurationSeconds);
    
    NSString *theFileName = [[filePath lastPathComponent] stringByDeletingPathExtension];
    NSString *xmlFileName = [theFileName stringByAppendingString:@".wav.xml"];
    NSLog(@"the File Name: %@", xmlFileName);
    
    NSString *startingText =[NSString stringWithFormat: @"<?xml version=\"1.0\" standalone=\"yes\"?>\n\
<NewDataSet>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>SpecialInstructionsLength</PropertyIdentifier>\n\
         <PropertyValue>0</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>DictationLength</PropertyIdentifier>\n\
         <PropertyValue>%.0f</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>CreationDateTime</PropertyIdentifier>\n\
         <PropertyValue>%@</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>LastModified</PropertyIdentifier>\n\
         <PropertyValue>0001-01-01 00:00:00</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Status</PropertyIdentifier>\n\
         <PropertyValue>2</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>LastUserName</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Encrypted</PropertyIdentifier>\n\
         <PropertyValue>False</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>SendNotifications</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>NotificationAskForAttachments</PropertyIdentifier>\n\
         <PropertyValue>False</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>NotificationReplyToAddresses</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Author</PropertyIdentifier>\n\
         <PropertyValue>Mobile</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>RevisionAuthor</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
        <PropertyIdentifier>Title</PropertyIdentifier>\n\
        <PropertyValue>%@</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Keyword</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Comments</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Priority</PropertyIdentifier>\n\
         <PropertyValue>0</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>EOL</PropertyIdentifier>\n\
         <PropertyValue>True</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>DPMKeywordContent1</PropertyIdentifier>\n\
         <PropertyValue>Supon Legal</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>DPMKeywordContent2</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>DPMKeywordContent3</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>DPMKeywordContent4</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>DPMKeywordContent5</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>DPMLabel1</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>DPMLabel2</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>DPMBarcode</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>DPMDownloadDate</PropertyIdentifier>\n\
         <PropertyValue>2017-10-11 11:03:29</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Delivery</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>DictationPosition</PropertyIdentifier>\n\
         <PropertyValue>0</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>TranscriptionPosition</PropertyIdentifier>\n\
         <PropertyValue>0</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Worktype</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Transcriptionist</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>DictationId</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Department</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Workstation</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Custom1</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Custom2</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Custom3</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Custom4</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Custom5</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>DictationHistory</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>MainDocumentFileName</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>IsSrDictation</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>IsQueuedForRecognition</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>QueuedForRecognitionBy</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>RecognitionPosition</PropertyIdentifier>\n\
         <PropertyValue>0</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>CorrectionPosition</PropertyIdentifier>\n\
         <PropertyValue>0</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>SRInformation</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>RecordedWith</PropertyIdentifier>\n\
         <PropertyValue>0</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>WordAnalyzingDone</PropertyIdentifier>\n\
         <PropertyValue>False</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>ReadOnlySimpleEditStrings</PropertyIdentifier>\n\
         <PropertyValue>0</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>Category</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>DictationType</PropertyIdentifier>\n\
         <PropertyValue>0</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>TranscriptionJobId</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>NumberOfSpeakers</PropertyIdentifier>\n\
         <PropertyValue>1</PropertyValue>\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>SELiveLastDictationErrorText</PropertyIdentifier>\n\
         <PropertyValue />\n\
    </DictationProperties>\n\
    <DictationProperties>\n\
         <PropertyIdentifier>SELiveLastDictationErrorCode</PropertyIdentifier>\n\
         <PropertyValue>0</PropertyValue>\n\
    </DictationProperties>\n\
</NewDataSet>", audioDurationSeconds, CreationDate, theFileName];
    
    NSError *error;
    NSString *filePath1 = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:xmlFileName];
    [startingText writeToFile:filePath1 atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
    NSLog(@"Created: %@",error);
    NSLog(@"%@", startingText);
    return YES;
}



@end

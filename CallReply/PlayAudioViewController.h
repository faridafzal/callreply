//
//  PlayAudioViewController.h
//  CallReply
//
//  Created by apple on 3/14/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecordingViewController.h"
#import <AVFoundation/AVFoundation.h>

#import "Audio.h"

@interface PlayAudioViewController : UIViewController<AVAudioRecorderDelegate, AVAudioPlayerDelegate>
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) Audio *audio;
@property (nonatomic,retain) AVAudioPlayer *player;

- (IBAction)playAudio:(id)sender;
- (IBAction)stopAudio:(id)sender;
@end

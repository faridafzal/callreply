//
//  ShowRecordingViewController.h
//  CallReply
//
//  Created by apple on 3/10/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "SWTableViewCell.h"

@interface ShowRecordingViewController : UIViewController<SWTableViewCellDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate, UITableViewDelegate>
- (IBAction)startRecording:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *cellArray;



@end

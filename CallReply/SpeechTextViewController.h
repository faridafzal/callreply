//
//  SpeechTextViewController.h
//  CallReply
//
//  Created by apple on 10/6/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Speech/Speech.h>
#import "Text.h"

@interface SpeechTextViewController : UIViewController <SFSpeechRecognizerDelegate, UITextViewDelegate, AVAudioPlayerDelegate>
{
    SFSpeechRecognizer *recognizer;
    AVAudioEngine *audioEngine;
    AVSpeechSynthesizer *speechSynthesizer;
    SFSpeechAudioBufferRecognitionRequest *recognitionRequest;
    SFSpeechRecognitionTask *currentTask;
    AVAudioInputNode *inputNode;
}
- (IBAction)makeUserSession:(id)sender;
- (IBAction)saveText:(id)sender;
@property BOOL isNewRecording;
@property Text *formerText;

@property (strong, nonatomic) AVAudioPlayer *player;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *microPhoneButton;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UIButton *printButton;
@property (strong, nonatomic) IBOutlet UILabel *pageNo;
@property (strong, nonatomic) IBOutlet UIButton *textViewTapButton;
@property (strong, nonatomic) IBOutlet UIButton *autoSaveButton;


- (IBAction)microPhoneTapped;
- (IBAction)clearTextView:(id)sender;
- (IBAction)printText:(id)sender;
- (IBAction)didTapAutoSave:(id)sender;

@end

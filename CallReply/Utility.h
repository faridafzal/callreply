//
//  Utility.h
//  CallReply
//
//  Created by apple on 10/26/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface Utility : NSObject
+ (BOOL) convertAudio: (NSString*)filePath;
+ (BOOL) createTextFile: (NSString*)filePath;
@end

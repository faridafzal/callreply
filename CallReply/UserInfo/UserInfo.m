
#import <UIKit/UIKit.h>
#import "UserInfo.h"



static UserInfo *singletonInstance;
@implementation UserInfo


-(void) copyObject :(UserInfo *) objToCopy {
	//Main
    singletonInstance.numberOfFreeTexts = objToCopy.numberOfFreeTexts;
    singletonInstance.numberOfFreeRecordings = objToCopy.numberOfFreeRecordings;
    singletonInstance.isSubscribed = objToCopy.isSubscribed;
    singletonInstance.isAutoSaveEnabled = objToCopy.isAutoSaveEnabled;
 }



#pragma mark - Custom Methods

-(void) loadUserInfo {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *myEncodedObject = [defaults objectForKey:kUserInfo];
	[defaults synchronize];
    UserInfo *obj = (UserInfo *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject];
	if (obj) {
		[self copyObject:obj];
	}
}

-(void)saveUserInfo {
	NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:singletonInstance];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:myEncodedObject forKey:kUserInfo];
    [defaults synchronize];
}

-(NSString*) nullCheck :(NSString *)str{if (str) {return str;}return @"";}


#pragma mark - Encode Decoder
- (void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeInt:[self numberOfFreeTexts] forKey:knumberOfFreeTexts];
    [encoder encodeInt:[self numberOfFreeRecordings] forKey:knumberOfFreeRecordings];
    [encoder encodeBool:[self isSubscribed] forKey:kisSubscribed];
    [encoder encodeBool:[self audioSessionForText] forKey:kaudioSessionForText];
    [encoder encodeBool:[self audioSessionForVoice] forKey:kaudioSessionForVoice];
    [encoder encodeBool:[self isAutoSaveEnabled] forKey:kautoSaveEnabled];
 }

- (id)initWithCoder:(NSCoder *)decoder {
	if((self = [super init])) {
        self.numberOfFreeTexts = [decoder decodeIntForKey:knumberOfFreeTexts];
        self.numberOfFreeRecordings = [decoder decodeIntForKey:knumberOfFreeRecordings];
        self.isSubscribed = [decoder decodeBoolForKey:kisSubscribed];
        self.audioSessionForVoice = [decoder decodeBoolForKey:kaudioSessionForVoice];
        self.audioSessionForText = [decoder decodeBoolForKey:kaudioSessionForText];
        self.isAutoSaveEnabled = [decoder decodeBoolForKey:kautoSaveEnabled];
    }
	return self;
}

-(void) setUserWithInfo:(NSDictionary *)userDict {
    self.numberOfFreeTexts = [[userDict valueForKey:knumberOfFreeTexts] intValue];
    self.numberOfFreeRecordings = [[userDict valueForKey:knumberOfFreeRecordings] intValue];
    self.isSubscribed = [userDict valueForKey:kisSubscribed];
    self.audioSessionForText = [userDict valueForKey:kaudioSessionForText];
    self.audioSessionForVoice = [userDict valueForKey:kaudioSessionForVoice];
    self.isAutoSaveEnabled = [userDict valueForKey:kautoSaveEnabled];
}


-(void) removeUserInfo{
    self.numberOfFreeTexts = 0;
    self.numberOfFreeRecordings = 0;
    self.isSubscribed = NO;
    self.audioSessionForVoice = NO;
    self.audioSessionForText = NO;
    self.isAutoSaveEnabled = NO;
    [self saveUserInfo];
}

#pragma mark - init
- (id) init {
    if (self = [super init]) {
	}
    self.isSubscribed = NO;
    self.isAutoSaveEnabled = NO;
    self.audioSessionForText = NO;
    self.audioSessionForVoice = NO;
    self.numberOfFreeTexts = 0;
    self.numberOfFreeRecordings = 0;
    return self;
}

#pragma mark - Shared Instance
+ (UserInfo*)instance {
    if(!singletonInstance) {
		singletonInstance=[[UserInfo alloc]init];
		[singletonInstance loadUserInfo];
	}
    return singletonInstance;
}
@end

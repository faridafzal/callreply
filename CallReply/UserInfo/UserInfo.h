

#define KStaticImageURL     @"http://www.uberguest.com/uberguest_dev/upload_image/no-image.jpg"

#define kUserInfo           @"userinfo"
#define knumberOfFreeTexts  @"numberOfFreeTexts"
#define knumberOfFreeRecordings @"numberOfFreeRecordings"
#define kisSubscribed           @"isSubscribed"
#define kaudioSessionForText    @"audioSessionForText"
#define kaudioSessionForVoice   @"audioSessionForVoice"
#define kautoSaveEnabled   @"autoSaveEnabled"

@interface UserInfo : NSObject
@property (nonatomic, assign) int numberOfFreeTexts;
@property (nonatomic, assign) int numberOfFreeRecordings;
@property BOOL isSubscribed;
@property BOOL audioSessionForText;
@property BOOL audioSessionForVoice;
@property BOOL isAutoSaveEnabled;
+ (UserInfo*)instance;

-(void) loadUserInfo;
-(void) saveUserInfo;
-(void) removeUserInfo;

@end

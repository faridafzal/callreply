//
//  RecordinglTableViewCell.h
//  CallReply
//
//  Created by Apple on 04/05/2017.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface RecordinglTableViewCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *audiofileName;
@property (weak, nonatomic) IBOutlet UIButton *shareAudio;

@end

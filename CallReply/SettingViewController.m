//
//  SettingViewController.m
//  CallReply
//
//  Created by apple on 5/17/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController ()

@end
#define kisPasswordEnabled  @"isPasswordEnabled"
@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSString *enablePassword = [[NSUserDefaults standardUserDefaults]
                           stringForKey:kisPasswordEnabled];
    if([enablePassword isEqualToString:@"no"])
        [self.passwordSwitch setOn:NO animated:YES];
    else
        [self.passwordSwitch setOn:YES animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.title = @"Voice Recording";
    self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backBtn"] style:UIBarButtonItemStylePlain target:self action:@selector(handleBack:)];
}

- (void)handleBack:(id)sender{    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)enablePassword:(id)sender {
    NSString *passwordEnable;;

    if([sender isOn]){
        passwordEnable = @"yes";
    }else{
        passwordEnable = @"no";

    }
    [[NSUserDefaults standardUserDefaults] setObject:passwordEnable forKey:kisPasswordEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (IBAction)ComposeMail {

    if (![MFMailComposeViewController canSendMail]) {
        UIAlertView *alertMessage=[[UIAlertView alloc] initWithTitle:@"" message:@"Mail Account is not configured" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertMessage show];
    }
    else
    {
        MFMailComposeViewController* composeVC = [[MFMailComposeViewController alloc] init];
        composeVC.mailComposeDelegate = self;

        // Configure the fields of the interface.
        [composeVC setToRecipients:[NSArray arrayWithObject:@"support@transcribeforme.ca"]];
        [composeVC setSubject:@"Contact Us"];
        NSString *emailbody = @"Contact Us";
        [composeVC setMessageBody:emailbody isHTML:NO];
        [self presentViewController:composeVC animated:YES completion:nil];
    }

}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    // Check the result or perform other tasks.

    // Dismiss the mail compose view controller.
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end

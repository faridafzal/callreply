//
//  SettingViewController.h
//  CallReply
//
//  Created by apple on 5/17/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface SettingViewController : UIViewController<MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UISwitch *passwordSwitch;
- (IBAction)enablePassword:(id)sender;
- (IBAction)ComposeMail;


@end

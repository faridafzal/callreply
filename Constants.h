#include <UIKit/UIDevice.h>


///iCloud keys
#define ksubscriptionDate       @"subscriptionDate"
#define kremainingDays          @"remainingDays"
#define kappState               @"appState"
#define kSuccess                @"Success"
#define kiCloudStatus           @"iCloudStatus"
#define kiCloudSyncTitle        @"iCloud Sync"
#define kiCloudSyncMessage      @"Please Sign In iCloud to retrieve previous subscription."
#define kproductIdentifier      @"productIdentifier"
#define kuserIsSubscribed       @"userIsSubscribed"

//User defaults
#define kisAppLatestVersion         @"isAppLatestVersion"
#define kisNotFirstTimeRun          @"isNotFirstTimeRun"
#define kExpireDate                 @"kExpireDate"
#define kNumberOfFreeTextsCount 10
#define knumberOfFreeRecordingsCount 10
